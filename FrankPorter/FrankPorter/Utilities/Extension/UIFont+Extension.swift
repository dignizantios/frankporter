//
//  UIFont+Extension.swift
//  EasywashVendor
//
//  Created by om on 6/15/18.
//  Copyright © 2018 dignizant. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case Boook = "FuturaBT-Book"
    case light = "FuturaBT-Light"
    
}

extension UIFont
{

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
