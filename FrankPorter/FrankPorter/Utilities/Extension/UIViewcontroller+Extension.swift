//
//  UIViewcontroller+Extension.swift
//  FrankPorter
//
//  Created by Khushbu on 07/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit


    
func GetYearNames() -> [String]
{
    var arrayNames : [String] = []
    
    let currentYear = GetCurrentYear()
    var startYear = currentYear - selectedMinimumYear
    var endYear = currentYear + selectedMaximumYear
    
    for i in startYear..<endYear
    {
        arrayNames.append("\(startYear)")
        startYear = startYear + 1
    }
    
    return arrayNames
}
func GetMaxCurrentYearNames() -> [String]
{
    var arrayNames : [String] = []
    
    let currentYear = GetCurrentYear()
    var startYear = currentYear - selectedMinimumYear
    var endYear = currentYear
    
    for i in startYear..<endYear+1
    {
        arrayNames.append("\(startYear)")
        startYear = startYear + 1
    }
    
    return arrayNames
    
    
}

func GetMaximumDate() -> Date
{
    let currentYear = GetCurrentYear()
    var endYear = currentYear + selectedMinimumYear
    
    var strMonth = ""
    let currentMonth = GetCurrentMonth()
    strMonth = currentMonth<10 ? "0\(currentMonth)" : "\(currentMonth)"
    
    let strDate = "31-12-\(endYear)"
    let date = stringTodate(Formatter: "dd-MM-yyyy", strDate:strDate)
//    print("maximum date - ",date)
    return date
    
}

func GetMinimumDate() -> Date
{
    let currentYear = GetCurrentYear()
    var endYear = currentYear - selectedMinimumYear
    
    var strMonth = ""
    let currentMonth = GetCurrentMonth()
    strMonth = currentMonth<10 ? "0\(currentMonth)" : "\(currentMonth)"
    
    let strDate = "01-01-\(endYear)"
    let date = stringTodate(Formatter: "dd-MM-yyyy", strDate:strDate)
//    print("maximum date - ",date)
    return date
    
}
func GetCurrentYear() -> Int
{
    let date = Date()
    let calendar = Calendar.current
    
    let year =  calendar.component(.year, from: date)
    return year
    
}
func GetCurrentMonth() -> Int
{
    let date = Date()
    let calendar = Calendar.current
    
    let month =  calendar.component(.month, from: date)
    return month
    
}

extension UIViewController
{
    func backGroundMessageView(strMsg:String = "No data found",textFont:UIFont = themeFont(size: 15, fontname: .Boook), textColor:UIColor = UIColor.appThemeDarkGreyColor) -> UIView
    {
        let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 10, y: 0, width: self.view.bounds.size.width - 20, height: self.view.bounds.size.height))
        noDataLabel.numberOfLines = 0
        noDataLabel.tag           = 97890
        noDataLabel.text          = strMsg
        noDataLabel.font          = textFont
        noDataLabel.textColor     = textColor
        noDataLabel.textAlignment = .center
        (self as? UITableView)?.separatorStyle = .none
        return noDataLabel
    }
}
