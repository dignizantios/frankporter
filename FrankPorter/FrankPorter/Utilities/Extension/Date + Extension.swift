//
//  Date + Extension.swift
//  SimpReg
//
//  Created by YASH on 17/07/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import Foundation

extension Date{
    
    
    
    func DatesAvailableBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date,SelectedDate:Date) ->Bool
    {
        var startDate = startDate
        let calendar = Calendar.current
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        while startDate <= endDate
        {
            if(startDate == SelectedDate)
            {
                return true
            }
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return false
    }
    func GetDateAfterDays(days:Int) -> Date
    {
        let calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.day = days // For next button
        let AfterDate = calendar.date(byAdding: dateComponents, to:self)
        return AfterDate ?? self
    }
}


extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        print("start - ",start)
        print("end - ",end)
        return end - start
    }
    func startOfMonth() -> Date? {
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month, .hour], from: Calendar.current.startOfDay(for: self))
        return Calendar.current.date(from: comp)!
    }
    
    func endOfMonth() -> Date? {
        var comp: DateComponents = Calendar.current.dateComponents([.month, .day, .hour], from: Calendar.current.startOfDay(for: self))
        comp.month = 1
        comp.day = -1
        return Calendar.current.date(byAdding: comp, to: self.startOfMonth()!)
    }
}



func generateDatesArrayBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date) ->[Date]
{
    var datesArray: [Date] =  [Date]()
    var startDate = startDate
    let calendar = Calendar.current
    
    let fmt = DateFormatter()
    fmt.dateFormat = OrinalDtFormater
//    print("startDate - ",startDate)
    while startDate < endDate {
        datesArray.append(startDate)
        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
        
    }
    return datesArray
}
