//
//  InvoiceManager.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class InvoiceManager: NSObject {

    fileprivate unowned var theController: InvoiceVC
    fileprivate var theModel:InvoiceModel
    
    init(theController:InvoiceVC, theModel:InvoiceModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}


//MARK:- Calendar components delegate

extension InvoiceManager : SelectCalendarComponentDelegate
{
    func NextButtonAction()
    {
        
        
        if(theModel.Month < 12)
        {
            theModel.Month = theModel.Month + 1
        }
            
        else{
            theModel.Month = 1
            theModel.Year = theModel.Year + 1
        }
        theController.NextMonthAction()
        
    }
    func PreviousButtonAction()
    {
        if(theModel.Month == 1)
        {
            theModel.Month = 12
            theModel.Year = theModel.Year - 1
            
        }else{
            theModel.Month = theModel.Month - 1
        }
        theController.PreviousMonthAction()
    }
    func SelectYearAction(strYear: String) {
        
        theModel.Year = Int(strYear) ?? GetCurrentYear()
        theController.SetDropdownSelection(text: strYear)
    }
    
    
}
