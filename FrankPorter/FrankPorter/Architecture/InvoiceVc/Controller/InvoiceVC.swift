//
//  InvoiceVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class InvoiceVC: UIViewController {

    var selectedParentVC =  CheckParentVC.invoice

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:InvoiceView = { [unowned self] in
        return self.view as! InvoiceView
        }()
    
    private lazy var theCurrentManager:InvoiceManager = { [unowned self] in
        return InvoiceManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:InvoiceModel = { [unowned self] in
        return InvoiceModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    func setupUI()
    {
        theCurrentView.setupUI(theDelegate: theCurrentManager)
        
        theCurrentView.SetSelectedMonthYear(strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")

    }
    
    //MARK:- Calendar dates selection methods
    func SetDropdownSelection(text: String)
    {
        GetInvoiceService()
    }
    func NextMonthAction()
    {
        theCurrentView.SetSelectedMonthYear(strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")

        GetInvoiceService()
    }
    func PreviousMonthAction()
    {
        theCurrentView.SetSelectedMonthYear(strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")

        GetInvoiceService()
    }
    
    func GetInvoiceData(dict:JSON)
    {
        print("Dict - ",dict)
        if(selectedParentVC == .invoice)
        {
//            setupNavigationbarwithBackButton(titleText: dict["nickname"].stringValue)
            setupNavigationbarwithHomeButton(titleText: dict["nickname"].stringValue)            
        }else{
            setupNavigationBarwithTitle(titleText: selectedParentVC.rawValue)
//            setupNavigationbarwithBackButton(titleText: selectedParentVC.rawValue)
        }
        theCurrentModel.dictData = dict
        GetInvoiceService()
    }
    //MARK:- Other methods
    
//    func SetSelectedMonth()
//    {
//        theCurrentView.SetCalendarMonthValue()
//    }
}

//MARK:- Service
extension InvoiceVC
{
    func GetInvoiceService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let URL = BaseServerURL + kGetInvoiceURL
            
            /*
             property_id [Like : 5b70e0fa99b485003e7e7fa6]
             Month [MM] Like (february : 02 december : 12)
             year  [YYYY] Like: 2018,2019
             lang [0 = English]
             */
            
            /*let param : [String:String] = ["property_id" : theCurrentModel.dictData["_id"].stringValue,
                                           "month" : theCurrentModel.Month < 10 ? "0\(theCurrentModel.Month)" : "\(theCurrentModel.Month)",
                "year" : "\(theCurrentModel.Year)",
                "lang" : GetApplicationLanguage()
            ]*/
            
            let param : [String:String] = ["property_id" : dictSelectedProperty["_id"].stringValue,
                                           "month" : theCurrentModel.Month < 10 ? "0\(theCurrentModel.Month)" : "\(theCurrentModel.Month)",
                "year" : "\(theCurrentModel.Year)",
                "lang" : GetApplicationLanguage()
            ]
            print("param - ",param)
            CommonService().ServerPostService(url: URL, parameters: param,completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["flag"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.theCurrentView.vwContent.isHidden = false
                        self.theCurrentView.SetUpServiceData(dict:data)
                        self.theCurrentView.vwNoData.isHidden = true

                    }
                    else{
                        self.theCurrentView.vwContent.isHidden = true
                        
                        self.theCurrentView.strMessage = json["msg"].stringValue
                        self.theCurrentView.lblError.text = json["msg"].stringValue
                        self.theCurrentView.vwNoData.isHidden = false
                    }
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
}


