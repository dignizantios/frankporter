//
//  InvoiceModel.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import SwiftyJSON

class InvoiceModel {
    
    private unowned var theController: InvoiceVC
    
    
    var Year = GetCurrentYear()
    var Month = GetCurrentMonth()
    var dictData = JSON()
    // MARK:- View Lifecycle
    init(theController:InvoiceVC) {
        self.theController = theController
    }
    
}
