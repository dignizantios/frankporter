//
//  InvoiceView.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
class InvoiceView: UIView {

    //MARK: - IBOutlet
    
    
    @IBOutlet weak var vwCalendarCompenents: UIView!

    @IBOutlet weak var lblEarningValue: UILabel!
    @IBOutlet weak var lblStayValue: UILabel!
    @IBOutlet weak var lblAvgNightRateValue: UILabel!
    @IBOutlet weak var lblOccupancyRateValue: UILabel!
    @IBOutlet weak var lblNightsValue: UILabel!
    
    
    @IBOutlet weak var lblEarning: UILabel!
    @IBOutlet weak var lblStay: UILabel!
    @IBOutlet weak var lblAvgNightRate: UILabel!
    @IBOutlet weak var lblOccupancyRate: UILabel!
    @IBOutlet weak var lblNights: UILabel!
    
    @IBOutlet weak var vwContent: UIView!

    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var vwNoData: UIView!

    //MARK: - Variable
    var arrMonthName = [String]()
    var strMessage = ""
    
    var vw = Bundle.main.loadNibNamed("CalendarMonthYearView", owner: self, options: nil)?[0] as? CalendarMonthYearView
    // MARK:- UI Methods
    func setupUI(theDelegate: InvoiceManager)
    {
        [lblEarning,lblStay,lblAvgNightRate,lblOccupancyRate,lblNights,lblError].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 16, fontname: .Boook)
        }
        [lblEarningValue,lblStayValue,lblAvgNightRateValue,lblOccupancyRateValue,lblNightsValue].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 14, fontname: .light)
        }
        
        lblError.textColor = UIColor.appThemeDarkGreyColor
        
        vwContent.isHidden = true
        vwNoData.isHidden = true
        vw?.frame = self.vwCalendarCompenents.frame
        vw?.CalendarComponentsDelegate = theDelegate
        self.vwCalendarCompenents.addSubview(vw ?? UIView())
    }
    func setTheDelegates(theDelegate: InvoiceManager)
    {
        
    }

    func SetSelectedMonthYear(strMonth:String,strYear:String)
    {
        vw?.SetCurrentMonthYear(strMonth: strMonth,strYear:strYear)
        
    }
    func SetUpServiceData(dict:JSON)
    {
        print("response data - ",dict)
        lblEarningValue.text = "\(AppCurrency) " + dict["earning"].stringValue
        lblStayValue.text = dict["stays"].stringValue
        lblAvgNightRateValue.text = dict["avg_night_rate"].stringValue
        lblOccupancyRateValue.text = dict["occupancy_rate"].stringValue + " %"
        lblNightsValue.text = dict["nights"].stringValue
        
    }
}
