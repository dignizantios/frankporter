//
//  SelectPropertyManager.swift
//  FrankPorter
//
//  Created by Khushbu on 07/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SDWebImage

class SelectPropertyManager: NSObject
{
    fileprivate unowned var theController: SelectPropertyVC
    fileprivate var theModel:SelectProperty
    
    init(theController:SelectPropertyVC, theModel:SelectProperty) {
        self.theController = theController
        self.theModel = theModel
    }
    
    
}
extension SelectPropertyManager: UITableViewDataSource,UITableViewDelegate
{
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(theModel.arrayProperty.count == 0)
        {
            tableView.backgroundView = theController.backGroundMessageView(strMsg: theModel.strMessage)
            return 0
        }
        tableView.backgroundView = nil
        return theModel.arrayProperty.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let dict = theModel.arrayProperty[indexPath.row]
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "PropertyTableViewCell") as! PropertyTableViewCell
        cell.selectionStyle = .none
//        cell.backgroundColor = .red
        
        let dictAddress = dict["address"]
        cell.lblHomeNumber.text = "\(dict["nickname"].stringValue)"
        cell.lblAddress.text = "\(dictAddress["full"].stringValue)"
        cell.lblNoOfBedroom.text = "\(dict["bedrooms"].stringValue) Bedroom"
        
        let dictPicture = dict["picture"]

        let str = "https:" + dictPicture["thumbnail"].stringValue
        cell.imgvwProperty.sd_setImage(with: URL(string:str.encodedURLString()), placeholderImage: UIImage(named: "square_image_placeholder"))

        if(dict["active"].boolValue)
        {
            cell.lblStatus.text = getCommonString(key: "Active_key")
        }else{
            cell.lblStatus.text = getCommonString(key: "Inactive_key")
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let dict = theModel.arrayProperty[indexPath.row]
//        theController.NavigateToController(dict:dict)
        dictSelectedProperty = theModel.arrayProperty[indexPath.row]
        theController.NavigateToHome()

    }
    
}
