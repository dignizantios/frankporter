//
//  SelectPropertyVC.swift
//  FrankPorter
//
//  Created by Khushbu on 07/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

enum CheckParentVC : String
{
    case calendar = "Calendar"
    case invoice = "Statistics"
    case statement = "Statement"
    case blockoffDates = "Block off dates"
}


class SelectPropertyVC: UIViewController {

    var selectedParentVC =  CheckParentVC.calendar
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.appThemeDarkGreyColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.GetPropertyList()
        refreshControl.endRefreshing()
        
    }
    
    
    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:SelectPropertyView = { [unowned self] in
        return self.view as! SelectPropertyView
        }()
    
    private lazy var theCurrentManager:SelectPropertyManager = { [unowned self] in
        return SelectPropertyManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:SelectProperty = { [unowned self] in
        return SelectProperty(theController:self)
        }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupManager()
        GetPropertyList()
        
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
       
    }
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationBarwithTitle(titleText: "Frank Porter")
        self.tabBarController?.tabBar.isHidden = true
        
       /* if(selectedParentVC == .statement || selectedParentVC == .blockoffDates)
        {
            setupNavigationbarwithBackButton(titleText: selectedParentVC.rawValue)
        }
        else{
            setupNavigationBarwithTitle(titleText: selectedParentVC.rawValue)
        }*/
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = true
        CommonService().cancelledAllRequest()
    }
    func setupManager()
    {
        self.setupUI()
        theCurrentView.tblProperty.addSubview(refreshControl)
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }

}
//MARK:- Navigation
extension SelectPropertyVC
{
    
    func NavigateToHome()
    {
        let objCustomTabBar = CustomTabbarVC()
        self.navigationController?.pushViewController(objCustomTabBar, animated: false)
    }
    
    func NavigateToController(dict:JSON)
    {
        if(selectedParentVC == .calendar)
        {
            let obj = mainStoryboard.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            isSetMaxCurrentYear = false
            obj.selectedParentVC = self.selectedParentVC
            obj.SetData(json: dict)
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if(selectedParentVC == .invoice)
        {
            let obj = mainStoryboard.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            isSetMaxCurrentYear = false
            obj.selectedParentVC = self.selectedParentVC
            obj.GetInvoiceData(dict: dict)
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if(selectedParentVC == .statement)
        {
            /*let obj = mainStoryboard.instantiateViewController(withIdentifier: "StatementVc") as! StatementVc
            obj.SetUpData(json: dict)
            isSetMaxCurrentYear = true            
            self.navigationController?.pushViewController(obj, animated: true)*/
        }
        else if(selectedParentVC == .blockoffDates)
        {
            let obj = mainStoryboard.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            isSetMaxCurrentYear = false
            obj.selectedParentVC = self.selectedParentVC
            obj.SetData(json: dict)
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
}
//MARK:- Other methods
extension SelectPropertyVC
{
    func LoadPropertyData(arrayProperty:[JSON])
    {
        theCurrentModel.arrayProperty = []
        theCurrentModel.arrayProperty = arrayProperty
        theCurrentView.ReloadProperty()
    }
}
//MARK:- Service
extension SelectPropertyVC
{
    func GetPropertyList()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let URL = BaseURL + kGetPropertyURL
            
            CommonService().CommonService(url: URL, completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    self.LoadPropertyData(arrayProperty:json["results"].arrayValue)
                    
                } else {
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
}
