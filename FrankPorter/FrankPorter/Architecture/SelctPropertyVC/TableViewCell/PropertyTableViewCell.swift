//
//  PropertyTableViewCell.swift
//  FrankPorter
//
//  Created by Khushbu on 07/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class PropertyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHomeNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgvwProperty: UIImageView!
    @IBOutlet weak var lblNoOfBedroom: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        imgvwProperty.layer.cornerRadius = 10
        imgvwProperty.layer.masksToBounds = true
        
        lblHomeNumber.textColor = UIColor.black
        lblHomeNumber.font = UIFont.systemFont(ofSize: 16)
        
        lblAddress.textColor = UIColor.appThemeLightGreyColor
        lblAddress.font = UIFont.systemFont(ofSize: 14)
        
        lblNoOfBedroom.textColor = UIColor.appThemeLightGreyColor
        lblNoOfBedroom.font = UIFont.systemFont(ofSize: 14)
        
        lblStatus.textColor = UIColor.black
        lblStatus.font = UIFont.systemFont(ofSize: 14)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
