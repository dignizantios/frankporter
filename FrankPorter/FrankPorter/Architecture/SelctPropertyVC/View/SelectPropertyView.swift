//
//  SelectPropertyView.swift
//  FrankPorter
//
//  Created by Khushbu on 07/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SelectPropertyView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var tblProperty : UITableView!

   
    
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        
        lblTitle.textColor = UIColor.black
        lblTitle.font = themeFont(size: 18, fontname: .Boook)
        
        
        tblProperty.register(UINib(nibName: "PropertyTableViewCell", bundle: nil), forCellReuseIdentifier: "PropertyTableViewCell")
        tblProperty.tableFooterView = UIView()
    }
    
    
    func setTheDelegates(theDelegate: SelectPropertyManager)
    {
        tblProperty.delegate = theDelegate
        tblProperty.dataSource = theDelegate
    }
    func ReloadProperty()
    {
        tblProperty.reloadData()
    }
    
}
