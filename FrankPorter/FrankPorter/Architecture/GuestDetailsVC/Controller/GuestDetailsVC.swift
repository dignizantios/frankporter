//
//  GuestDetailsVC.swift
//  FrankPorter
//
//  Created by Khushbu on 13/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
class GuestDetailsVC: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:GuestDetailsView = { [unowned self] in
        return self.view as! GuestDetailsView
        }()
    
    private lazy var theCurrentManager:GuestDetailsManager = { [unowned self] in
        return GuestDetailsManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:GuestDetailsModel = { [unowned self] in
        return GuestDetailsModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarwithBackButton(titleText: "Booking Details")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }
    func SetUpData(json : JSON)
    {
        theCurrentModel.dictDetails = json
        theCurrentView.SetUpData(json: json)
    }


}
