//
//  GuestDetailsView.swift
//  FrankPorter
//
//  Created by Khushbu on 13/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
class GuestDetailsView: UIView {

    
    var strFormatter = "yyyy-MM-dd"
    var strFormatter1 = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX"
    var strFormatter2 = "MMMM dd,yyyy"
    var strFormatter3 = "yyyy-MM-dd HH:mm:ss"

    let gregorian = Calendar(identifier: .gregorian)
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX"
        return formatter
    }()
    let formatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMMM-yyyy"
        return formatter
    }()
    let formatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    @IBOutlet var lblGuestDetailsHeading : UILabel!
    @IBOutlet var lblBookingDetailsHeading : UILabel!
    @IBOutlet var lblPaymentHeading : UILabel!
    
    @IBOutlet var lblGuestName : UILabel!
    
    @IBOutlet var lblArrival : UILabel!
    @IBOutlet var lblArrivalDate : UILabel!

    @IBOutlet var lblDeparture : UILabel!
    @IBOutlet var lblDepartureDate : UILabel!

    @IBOutlet var lblNightsHeading : UILabel!
    @IBOutlet var lblNights : UILabel!

    @IBOutlet var lblNoOfGuestHeading : UILabel!
    @IBOutlet var lblNoOfGuest : UILabel!
    
    @IBOutlet var lblTotalPaymentHeading : UILabel!
    @IBOutlet var lblTotalPayment : UILabel!
    @IBOutlet var imgBookingSource: UIImageView!
    
    func setupUI()
    {
        [lblGuestDetailsHeading,lblBookingDetailsHeading,lblPaymentHeading].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .light)
            lbl?.textColor = UIColor.black
        }
        
        lblGuestName.font = themeFont(size: 20, fontname: .Boook)
        lblGuestName.textColor = UIColor.black
        
        
        [lblArrival,lblDeparture,lblNightsHeading,lblNoOfGuestHeading,lblTotalPaymentHeading].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .light)
            lbl?.textColor = UIColor.appThemeLightGreyColor
        }
        
        [lblNights,lblNoOfGuest,lblTotalPayment].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .light)
            lbl?.textColor = UIColor.black
        }
        
        [lblArrivalDate,lblDepartureDate].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .Boook)
            lbl?.textColor = UIColor.black

        }
        
    }
    
    
    func setTheDelegates(theDelegate: GuestDetailsManager)
    {
        
    }

    func SetUpData(json : JSON)
    {
        print("Details json - ",json)
        
        let dict = json
        let dictBooking = dict["reservation"]
        
        let strCheckIn = dictBooking["checkIn"].stringValue
        let strCheckOut = dictBooking["checkOut"].stringValue
       
        let convertedStrCheckIn = stringTodate(OrignalFormatter: strFormatter1, YouWantFormatter: strFormatter2, strDate: strCheckIn)
        let convertedStrCheckOut = stringTodate(OrignalFormatter: strFormatter1, YouWantFormatter: strFormatter2, strDate: strCheckOut)
        
        lblArrivalDate.text = convertedStrCheckIn
        lblDepartureDate.text = convertedStrCheckOut

        let dictGuest = dictBooking["guest"]
        lblGuestName.text = dictGuest["fullName"].stringValue
        lblNights.text = dictBooking["nightsCount"].stringValue
        lblNoOfGuest.text = dictBooking["guestsCount"].stringValue        
        
        imgBookingSource.image = GetSelectedSource(strSource: dictBooking["source"].stringValue)
        
        let dictPayment = dictBooking["money"]
        lblTotalPayment.text = dictPayment["currency"].stringValue + " " + dictPayment["ownerRevenue"].stringValue

        if(dict["ownersReservation"].exists())
        {
            lblGuestName.text = "Owner Stay"
            
//            let dictBooking = dict["ownersReservation"]
            
            let strCheckIn = dict["checkIn"].stringValue
            let strCheckOut = dict["checkOut"].stringValue
            
            let convertedStrCheckIn = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter2, strDate: strCheckIn)
            let convertedStrCheckOut = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter2, strDate: strCheckOut)
            
            lblArrivalDate.text = convertedStrCheckIn
            lblDepartureDate.text = convertedStrCheckOut
            
            //
            let checkInDate = stringTodate(Formatter: strFormatter3, strDate: strCheckIn)
            let checkOutDate = stringTodate(Formatter: strFormatter3, strDate: strCheckOut)

            let nightsDates = generateDatesArrayBetweenTwoDates(OrinalDtFormater: strFormatter3, startDate: checkInDate, endDate: checkOutDate)
            lblNights.text = "\(nightsDates.count)"
            lblNoOfGuest.text = "0"
            lblTotalPayment.text = "0.0"

        }
        
        
        
        
    }
}
