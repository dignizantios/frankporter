//
//  GuestDetailsModel.swift
//  FrankPorter
//
//  Created by Khushbu on 13/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import SwiftyJSON

class GuestDetailsModel {
    
    private unowned var theController: GuestDetailsVC
    
    var dictDetails = JSON()
    
    // MARK:- View Lifecycle
    init(theController:GuestDetailsVC) {
        self.theController = theController
    }
    
}
