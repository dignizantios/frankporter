//
//  GuestDetailsManager.swift
//  FrankPorter
//
//  Created by Khushbu on 13/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class GuestDetailsManager: NSObject {

    
    fileprivate unowned var theController: GuestDetailsVC
    fileprivate var theModel:GuestDetailsModel
    
    init(theController:GuestDetailsVC, theModel:GuestDetailsModel) {
        self.theController = theController
        self.theModel = theModel
    }
}
