//
//  ChangePasswordView.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class ChangePasswordView: UIView {

    //MARK:- IBOutlet
    
    @IBOutlet weak var txtOldPassword: CustomTextField!
    
    @IBOutlet weak var txtNewPassword: CustomTextField!
    
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    @IBOutlet weak var btnSubmit: CustomButton!
    
    // MARK:- UI Methods
    func setupUI()
    {
        [txtOldPassword,txtNewPassword,txtConfirmPassword].forEach { (txtfld) in
            txtfld?.backgroundColor = UIColor.appThemeBackgroundLightColor
            txtfld?.layer.borderColor = (UIColor.appThemeLightGreyColor).cgColor
            txtfld?.layer.borderWidth = 0.5
            txtfld?.leftPaddingView = 18
            txtfld?.font = themeFont(size: 17, fontname: .light)
            
        }
        
        btnSubmit.titleLabel?.font = themeFont(size: 17, fontname: .light)

    }
    
    
    func setTheDelegates(theDelegate: ChangePasswordManager)
    {
        
    }
    

}
