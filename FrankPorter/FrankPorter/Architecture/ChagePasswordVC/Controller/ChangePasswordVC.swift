//
//  ChangePasswordVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ChangePasswordVC: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:ChangePasswordView = { [unowned self] in
        return self.view as! ChangePasswordView
        }()
    
    private lazy var theCurrentManager:ChangePasswordManager = { [unowned self] in
        return ChangePasswordManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:ChangePasswordModel = { [unowned self] in
        return ChangePasswordModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Change Password")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }
    
    
    @IBAction func btnSubmitTapped(_ sender: UIButton)
    {
        if(theCurrentView.txtOldPassword.text?.isEmpty ?? false)
        {
            makeToast(strMessage: "Please enter old password")
            return
        }
        else if(theCurrentView.txtNewPassword.text?.isEmpty ?? false)
        {
            makeToast(strMessage: "Please enter new password")
            return
        }
        else if(theCurrentView.txtConfirmPassword.text?.isEmpty ?? false)
        {
            makeToast(strMessage: "Please enter confirm password")
            return
        }
        else if(theCurrentView.txtConfirmPassword.text != theCurrentView.txtNewPassword.text)
        {
            makeToast(strMessage: "Please enter new and confirm password same")
            return
        }
        else if(theCurrentView.txtOldPassword.text == theCurrentView.txtNewPassword.text)
        {
            makeToast(strMessage: "Please enter old and new password different")
            return
        }
        else{
            self.view.endEditing(true)
            ChangePasswordService()
        }
    }
}
//MARK:- Service
extension ChangePasswordVC
{
    func ChangePasswordService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let ChangePasswordURL = BaseURL + kChangePasswordURL
            
            let param : [String:String] = ["username" : getUserDetail("email"),
                                           "apiKey" : GuestyAPIKey,
                                           "password" : theCurrentView.txtOldPassword.text ?? "",
                                           "newPassword" : theCurrentView.txtNewPassword.text ?? ""
                                          ]
            print("ChangePasswordURL - ",ChangePasswordURL)

            print("param - ",param)
            
            CommonService().PostService(url: ChangePasswordURL,parameters: param, completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    guard let rowdata = try? json.rawData() else {return}
                    Defaults.setValue(rowdata, forKey: kAuthenticateKey)
                    Defaults.synchronize()
                    makeToast(strMessage: "Password changed successfully")
                    
//                    self.navigationController?.popViewController(animated: true)
                    let obj = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    let navigationController = UINavigationController(rootViewController: obj)
                    appdelgate.window?.rootViewController = navigationController
                    
                    Defaults.removeObject(forKey: kUserDetailsKey)
                    Defaults.synchronize()
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
        
    }
}
