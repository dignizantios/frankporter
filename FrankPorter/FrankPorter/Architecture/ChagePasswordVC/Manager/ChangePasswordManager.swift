//
//  ChangePasswordManager.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class ChangePasswordManager: NSObject {

    fileprivate unowned var theController: ChangePasswordVC
    fileprivate var theModel:ChangePasswordModel
    
    init(theController:ChangePasswordVC, theModel:ChangePasswordModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}
