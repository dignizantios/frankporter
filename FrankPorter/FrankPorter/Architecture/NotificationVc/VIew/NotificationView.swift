//
//  NotificationView.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class NotificationView: UIView {

    //MARK: - IBOutlet
    
    
    @IBOutlet weak var tblNotification: UITableView!
    
    
    // MARK:- UI Methods
    func setupUI()
    {
        tblNotification.register(UINib(nibName: "NotificationTableCell", bundle: nil), forCellReuseIdentifier: "NotificationTableCell")

        tblNotification.tableFooterView = UIView()
    }
    
    
    func setTheDelegates(theDelegate: NotificationManager)
    {
        tblNotification.delegate = theDelegate
        tblNotification.dataSource = theDelegate
    }

}
