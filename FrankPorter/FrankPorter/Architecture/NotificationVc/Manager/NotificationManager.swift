//
//  NotificationManager.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class NotificationManager: NSObject {

    fileprivate unowned var theController: NotificationVC
    fileprivate var theModel:NotificationModel
    
    init(theController:NotificationVC, theModel:NotificationModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}


extension NotificationManager: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell") as! NotificationTableCell
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


