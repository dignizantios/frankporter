//
//  NotificationVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

  
    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:NotificationView = { [unowned self] in
        return self.view as! NotificationView
        }()
    
    private lazy var theCurrentManager:NotificationManager = { [unowned self] in
        return NotificationManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:NotificationModel = { [unowned self] in
        return NotificationModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Notifications")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }


}
