//
//  BookingDetailsVC.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class BookingDetailsVC: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:BookingDetailsView = { [unowned self] in
        return self.view as! BookingDetailsView
        }()
    
    private lazy var theCurrentManager:BookingDetailsManager = { [unowned self] in
        return BookingDetailsManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:BookingDetailsModel = { [unowned self] in
        return BookingDetailsModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarwithBackButton(titleText: "Booking Details")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }


}
