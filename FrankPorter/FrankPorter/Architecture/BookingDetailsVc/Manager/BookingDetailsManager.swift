//
//  BookingDetailsManager.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class BookingDetailsManager: NSObject {

    fileprivate unowned var theController: BookingDetailsVC
    fileprivate var theModel:BookingDetailsModel
    
    init(theController:BookingDetailsVC, theModel:BookingDetailsModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}
