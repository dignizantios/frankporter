//
//  BookingDetailsView.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class BookingDetailsView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var lblGuestDetails: UILabel!
    @IBOutlet weak var lblGuestName: UILabel!
    
    @IBOutlet weak var lblBookingDetails: UILabel!
    @IBOutlet weak var lblArrival: UILabel!
    @IBOutlet weak var lblArrivalDate: UILabel!
    @IBOutlet weak var lblDeparture: UILabel!
    @IBOutlet weak var lblDepartureDate: UILabel!
    @IBOutlet weak var lblDaysAndNights: UILabel!
    @IBOutlet weak var lbDaysAndNightsValue: UILabel!
    @IBOutlet weak var lblNoOfGuest: UILabel!
    @IBOutlet weak var lblNoOfGuestValue: UILabel!
    
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var lblTotalPayment: UILabel!
    @IBOutlet weak var lblTotalPaymentValue: UILabel!
    
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        [lblGuestDetails,lblBookingDetails,lblPayment].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.tintColor = UIColor.appThemeDarkGreyColor
        }
        
        lblGuestName.font = themeFont(size: 16, fontname: .light)
        lblGuestName.textColor = UIColor.black
        
        [lblArrival,lblDeparture,lblDaysAndNights,lblNoOfGuest,lblTotalPayment].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.textColor = UIColor.appThemeLightGreyColor
        }
        
        [lblArrivalDate,lblDepartureDate,lbDaysAndNightsValue,lblNoOfGuestValue,lblTotalPaymentValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.tintColor = .black
        }
        
        
    }
    
    
    func setTheDelegates(theDelegate: BookingDetailsManager)
    {
        
    }

}
