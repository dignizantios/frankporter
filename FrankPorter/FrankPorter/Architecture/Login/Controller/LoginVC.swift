//
//  LoginVC.swift
//  FrankPorter
//
//  Created by YASH on 27/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class LoginVC: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:LoginView = { [unowned self] in
        return self.view as! LoginView
        }()
    
    private lazy var theCurrentManager:LoginManager = { [unowned self] in
        return LoginManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:LoginModel = { [unowned self] in
        return LoginModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(getUserDetail("accountId") != "")
        {
            NavigateToHome()
        }
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI(theDelegate: theCurrentManager)
    }
    
}

extension LoginVC
{
    
    //MARK: - IBAction
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        print("Sign in")
        

        if(theCurrentView.txtEmail.text?.isEmpty ?? false)
        {
            makeToast(strMessage: getCommonString(key: "Enter_email_address_key"))
        }
        else if(theCurrentView.txtPassword.text?.isEmpty ?? false)
        {
            makeToast(strMessage: getCommonString(key: "Eneter_password_key"))

        }
        else if(!(theCurrentView.txtEmail.text?.isValidEmail() ?? false))
        {
            makeToast(strMessage: getCommonString(key: "Enter_valid_email_key"))
            
        }
        else{
            LoginService()
        }
        
    }
    
    
    @IBAction func btnFaceBookTapped(_ sender: UIButton) {
        print("FaceBook")
    }
    
}
//MARK:- Other Methods
extension LoginVC
{
    func NavigateToHome()
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SelectPropertyVC") as! SelectPropertyVC
        self.navigationController?.pushViewController(obj, animated: false)
//        appdelgate.objCustomTabBar = CustomTabbarVC()
//        self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: true)
//        self.navigationController?.present(appdelgate.objCustomTabBar, animated: true, completion: nil)

    }
}

//MARK:- Service
extension LoginVC
{
    
    func LoginService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let LoginURL = BaseURL + kLoginURL
            let param : [String:String] = ["username" : self.theCurrentView.txtEmail.text ?? "",
                                           "password" : self.theCurrentView.txtPassword.text ?? "",
                                           "apiKey" : GuestyAPIKey
                                ]
            CommonService().AuthenticateService(url: LoginURL, param: param, completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if json[kToken].exists(){
                        guard let rowdata = try? json.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: kAuthenticateKey)
                        Defaults.synchronize()
                        self.view.endEditing(true)
                        
                        self.GetUserDetails()
                    }
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
    func GetUserDetails()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let UserDetailsURL = BaseURL + kUserDetailsURL
            
            
            CommonService().CommonService(url: UserDetailsURL, completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    guard let rowdata = try? json.rawData() else {return}
                    Defaults.setValue(rowdata, forKey: kUserDetailsKey)
                    Defaults.synchronize()
                    self.view.endEditing(true)
                    
                    self.NavigateToHome()
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
}
