//
//  LoginManager.swift
//  FrankPorter
//
//  Created by YASH on 27/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//


import UIKit

class LoginManager: NSObject {

    fileprivate unowned var theController: LoginVC
    fileprivate var theModel:LoginModel
    
    init(theController:LoginVC, theModel:LoginModel) {
        self.theController = theController
        self.theModel = theModel
    }
}

//MARK:- Textfield delegate
extension LoginManager : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        theController.view.endEditing(true)
        return true
    }
    
}
