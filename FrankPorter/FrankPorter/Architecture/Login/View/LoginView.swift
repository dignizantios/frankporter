//
//  LoginView.swift
//  FrankPorter
//
//  Created by YASH on 27/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class LoginView: UIView {

    //MARK: - outlet
    
    @IBOutlet weak var lblSignin: UILabel!
    
    @IBOutlet weak var constantDistanceBetweenLblandView: NSLayoutConstraint!
    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSIgnin: CustomButton!
    @IBOutlet weak var btnFacebook: CustomButton!
    @IBOutlet weak var lblOrlogionWith: UILabel!
    
    // MARK:- UI Methods
    func setupUI(theDelegate:LoginManager)
    {
        
        lblSignin.font = themeFont(size: 24, fontname: .light)
        lblOrlogionWith.font = themeFont(size: 13, fontname: .light)
        
        if UIScreen.main.bounds.width <= 320
        {
           constantDistanceBetweenLblandView.constant = 30
        }
        else
        {
            constantDistanceBetweenLblandView.constant = 60
        }
        
        
        [txtEmail,txtPassword].forEach { (txtfld) in
            txtfld?.backgroundColor = UIColor.appThemeBackgroundLightColor
            txtfld?.layer.borderColor = (UIColor.appThemeLightGreyColor).cgColor
            txtfld?.layer.borderWidth = 0.5
            txtfld?.leftPaddingView = 18
            txtfld?.font = themeFont(size: 17, fontname: .light)
            txtfld?.delegate = theDelegate
//            txtfld?.addTarget(theDelegate, action: #selector(theDelegate.textFieldDidChange(_:)), for: .editingChanged)
        }
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        
        btnSIgnin.titleLabel?.font = themeFont(size: 17, fontname: .light)
        btnFacebook.titleLabel?.font = themeFont(size: 17, fontname: .light)
        btnForgotPassword.titleLabel?.font = themeFont(size: 16, fontname: .light)
        btnForgotPassword.setTitleColor(UIColor.black, for: .normal)
        
        btnSIgnin.setTitle(getCommonString(key: "Sign_in_key").uppercased(), for: .normal)
        btnForgotPassword.setTitle("\(getCommonString(key: "Forgot_password_key"))?", for: .normal)
        
        lblSignin.text = getCommonString(key: "Sign_in_key").uppercased()
        
    }
    
    
    func setTheDelegates(theDelegate: LoginManager)
    {
       
    }

}
