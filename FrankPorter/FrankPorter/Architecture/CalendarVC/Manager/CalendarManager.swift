//
//  CalendarManager.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftyJSON

enum BookingSource : String
{
    case frankporter = "Direct"
    case bookingDotCom = "Booking.com"
    case airbnb = "Airbnb"
    case tripAdviser = "tripadvisor"
}

func  GetSelectedSource(strSource : String) -> UIImage
{
    switch strSource
    {
        case BookingSource.frankporter.rawValue:
            return UIImage(named:"logo_login") ?? UIImage()
        case BookingSource.bookingDotCom.rawValue:
            return UIImage(named:"booking") ?? UIImage()
        case BookingSource.airbnb.rawValue:
            return UIImage(named:"logo_airbnb") ?? UIImage()
        case BookingSource.airbnb.rawValue:
            return UIImage(named:"logo_trip_adviser") ?? UIImage()
        default:
            return UIImage(named:"logo_login") ?? UIImage()
    }
}

class CalendarManager: NSObject {

    fileprivate unowned var theController: CalendarVC
    fileprivate var theModel:CalendarModel
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d"
        return formatter
    }()
    
    init(theController:CalendarVC, theModel:CalendarModel) {
        self.theController = theController
        self.theModel = theModel
        
    }
}


extension CalendarManager: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if theModel.dictInvoice.count == 0{
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "InvoiceCell") as! InvoiceCell
        let dict = theModel.dictInvoice
        cell.lblEarningValue.text = "\(AppCurrency) " + dict["earning"].stringValue
        cell.lblStayValue.text = dict["stays"].stringValue
        cell.lblAvgNightRateValue.text = dict["avg_night_rate"].stringValue
        cell.lblOccupancyRateValue.text = dict["occupancy_rate"].stringValue + " %"
        cell.lblNightsValue.text = dict["nights"].stringValue
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}

//MARK:- Calendar  delegate

extension CalendarManager : FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance
{
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return GetMaximumDate()        
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return GetMinimumDate()
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar)
    {
        print("Calendar page change")
        
        let selectedDates = calendar.selectedDates
        
        selectedDates.forEach { (date) in
            calendar.deselect(date)
        }
        theController.ResetCalendarData()
        theController.SetSelectedMonth()
        theController.GetBookingDataService()
        if theController.selectedParentVC == .calendar{
            theController.GetInvoiceService()
        }
    }
    
    //MARK: - Calendar Method
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String?
    {
        let strDate =  "     " + formatter.string(from: date)
        return strDate
    }
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(theController.formatter.string(from: date))")
//        print("arrayReservartion \(theModel.arrayReservations)")
        let currentDate = Date()
        
        let strDate = theController.formatter.string(from: date)
        var dictData = JSON()
        let dictDate = returnJSON(strDate: strDate)
        print("dictDate - ",dictDate)
        
        if(theController.selectedParentVC == .blockoffDates)
        {
            if date < currentDate {
                return
            }
            
            var status = BookingStatus.available.rawValue
            if theModel.arrayReservations.contains(where: { (json) -> Bool in
                
                if(json["date"].stringValue == strDate && (json["status"].stringValue == BookingStatus.unavailable.rawValue || json["status"].stringValue == BookingStatus.available.rawValue))
                {
                    dictData = returnJSON(strDate: strDate)
                    status = json["status"].stringValue
                    if(json["ownersReservation"].exists())
                    {
                        return true
                    }
                    return false
                    
                }
                return false
            }){
               
                print("Selected date json - ",dictData)
                switch(status)
                {
                    case BookingStatus.available.rawValue:
                        print("Show Date selection popup")
                        theController.ShowBlockPopUp(FromDate:date,isBlock:true,dictData:dictData)

                    case BookingStatus.unavailable.rawValue:
                        print("Show Date unselection popup")
                        theController.ShowBlockPopUp(FromDate:date,isBlock:false,dictData:dictData)

                    default:
                        print("Show Date selection popup")
                }
                
            }
            else
            {
                if(dictDate["status"].stringValue == BookingStatus.available.rawValue)
                {
                    print("Show Date selection popup")
                    theController.ShowBlockPopUp(FromDate:date,isBlock:true,dictData:dictDate)
                }
            }
            
        } else if theController.selectedParentVC == .calendar
        {
            if dictDate["status"].stringValue == BookingStatus.booked.rawValue{
                
                theController.NavigateToDetails(json: dictDate)
            }
            else if(dictDate["status"].stringValue == BookingStatus.unavailable.rawValue && dictDate["ownersReservation"].exists())
            {
                theController.NavigateToDetails(json: dictDate)
            }
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        
        print("did deselect date \(theController.formatter.string(from: date))")
        
        let strDate = theController.formatter.string(from: date)
        let dictDate = returnJSON(strDate: strDate)
        let currentDate = Date()
        
        if(theController.selectedParentVC == .blockoffDates)
        {
            if date < currentDate {
                return
            }
            var statusJSON = JSON()
            if theModel.arrayReservations.contains(where: { (json) -> Bool in
                
                print("status - ",json["status"].stringValue)
                print("json - ",json)

                if(json["date"].stringValue == strDate &&  json["status"].stringValue == BookingStatus.unavailable.rawValue)
                {

                    statusJSON = returnJSON(strDate: json["date"].stringValue)
                    if(json["ownersReservation"].exists())
                    {
                        return true
                    }
                    return false
                    
                }
                return false
            }){
//                print("statusJSON - ",statusJSON["status"].stringValue)
                print("Show Date unselection popup")
                print("Unselected date json - ",statusJSON)

                theController.ShowBlockPopUp(FromDate:date,isBlock:false,dictData:statusJSON)

            } else {
                theController.ShowBlockPopUp(FromDate:date,isBlock:true,dictData:statusJSON)
            }
        }
        else if theController.selectedParentVC == .calendar {
            if dictDate["status"].stringValue == BookingStatus.booked.rawValue{
                theController.NavigateToDetails(json: dictDate)
            }
            else if(dictDate["status"].stringValue == BookingStatus.unavailable.rawValue && dictDate["ownersReservation"].exists())
            {
                theController.NavigateToDetails(json: dictDate)
            }
        }
    }
    
}
//MARK:- Scroll Methods
extension CalendarManager
{
    
}
//MARK:- Other methods
extension CalendarManager
{
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        // Configure selection layer
        let diyCell = (cell as? DIYCalendarCell)
        
        let previousDate = theController.gregorian.date(byAdding: .day, value: -1, to: date)!
        let nextDate = theController.gregorian.date(byAdding: .day, value: 1, to: date)!
        
        let strDate = theController.formatter.string(from: date)
        /*let strDateBefore = theController.formatter.string(from: date.GetDateAfterDays(days: -1))
        let strDateAfter = theController.formatter.string(from: date.GetDateAfterDays(days: 1))*/
        
        let strpreviousDate = theController.formatter.string(from: previousDate)
        let strnextDate = theController.formatter.string(from: nextDate)
        
        var color = UIColor.clear
        diyCell?.colorsRightView = color
        diyCell?.colorsLeftView = color
        diyCell?.colorsView = color
        
        if theModel.arrayReservations.contains(where: { (json) -> Bool in
            if(json["date"].stringValue == strDate)
            {
                let index = theModel.arrayReservations.index(of: json)
                color = theModel.arrayDIYColors[index ?? 0]
                return true
            }
            return false
        }){
            diyCell?.colorsView = color
        }
       
        ///---------------------
        
        if position == .current
        {
            var selectionType = SelectionType.none
            let prevoiusDateJSON = returnJSON(strDate: strpreviousDate)
            let nextDateJSON = returnJSON(strDate: strnextDate)
            let currentDateJSON = returnJSON(strDate: strDate)
            
            if theController.GetCurrentCalendar().selectedDates.contains(date)
            {
                
                if(currentDateJSON["status"].stringValue == BookingStatus.unavailable.rawValue)
                {
                    
                    if(currentDateJSON["ownersReservation"].exists())
                    {
                        //===========For Owner reservation bookings=======//
                        
                        if(prevoiusDateJSON["id"] == currentDateJSON["id"] && nextDateJSON["id"] == currentDateJSON["id"])
                        {
                            diyCell?.colorsView = color
                            selectionType = .middle
                        }
                        else if(prevoiusDateJSON["id"] == currentDateJSON["id"] &&  nextDateJSON["id"] != currentDateJSON["id"])
                        {
                            diyCell?.colorsView = color
                            selectionType = .middle
                        }
                        else if(prevoiusDateJSON["id"] != currentDateJSON["id"] && CheckDateIsSelected(date: previousDate))
                        {
                            diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                            diyCell?.colorsRightView = color
                            selectionType = .bothLeftRightBorder
                        }
                        else if(CheckDateIsSelected(date: previousDate) == false)
                        {
                            diyCell?.colorsRightView = color
                            selectionType = .singleLeftBorder
                        }
                        else if(prevoiusDateJSON["id"] != currentDateJSON["id"] &&  CheckDateIsSelected(date: previousDate) == true)
                        {
                            diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                            diyCell?.colorsRightView = color
                            
                            selectionType = .bothLeftRightBorder
                        }
                        else if(CheckDateIsSelected(date: nextDate) == false && currentDateJSON["status"].stringValue == BookingStatus.available.rawValue)
                        {
                            diyCell?.colorsLeftView = color
                            selectionType = .singleRightBorder
                        }
                        
                    }else{
                        //===========For unavailable reservation bookings=======//
                        if(prevoiusDateJSON["status"] == currentDateJSON["status"] && nextDateJSON["status"] == currentDateJSON["status"])
                        {
                            if(currentDateJSON["ownersReservation"].exists() && !prevoiusDateJSON["ownersReservation"].exists())
                            {
                                diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                                diyCell?.colorsRightView = color
                                
                                selectionType = .bothLeftRightBorder
                            }
                            else if(prevoiusDateJSON["ownersReservation"].exists() && !currentDateJSON["ownersReservation"].exists())
                            {
                                diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                                diyCell?.colorsRightView = color
                                
                                selectionType = .bothLeftRightBorder
                            }else{
                                diyCell?.colorsView = color
                                selectionType = .middle
                            }
                        }
                        else if(theController.GetCurrentCalendarEndDate() == date)
                        {
                            diyCell?.colorsView = color
                            selectionType = .rightBorder
                        }
                        else if(CheckDateIsSelected(date: previousDate) == false)
                        {
                            diyCell?.colorsRightView = color
                            selectionType = .singleLeftBorder
                        }
                        else if(CheckDateIsSelected(date: date) == true && CheckDateIsSelected(date: previousDate) == true)
                        {
                            if(prevoiusDateJSON["status"] != currentDateJSON["status"])
                            {
                                diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                                diyCell?.colorsRightView = color
                                selectionType = .bothLeftRightBorder
                            }
                            else{
                                diyCell?.colorsView = color
                                selectionType = .middle
                            }
                        }
                        else{
                            diyCell?.colorsRightView = color
                            selectionType = .singleLeftBorder
                        }
                    }
                }
                else{
                   
                    //===========For Other user bookings=======//
                    if(prevoiusDateJSON["id"] == currentDateJSON["id"] && nextDateJSON["id"] == currentDateJSON["id"])
                    {
                        if(theController.GetCurrentCalendarStartDate() == date)
                        {
                            diyCell?.colorsView = GetDateColor(date: nextDate)
                            
                        }else{
                            diyCell?.colorsView = GetDateColor(date: previousDate)

                        }
                        selectionType = .middle
                    }
                    else if(prevoiusDateJSON["id"] == currentDateJSON["id"] &&  nextDateJSON["id"] != currentDateJSON["id"])
                    {
                        diyCell?.colorsView = color
                        selectionType = .middle
                    }
                    else if(prevoiusDateJSON["id"] != currentDateJSON["id"] &&  nextDateJSON["id"] != currentDateJSON["id"] && CheckDateIsSelected(date: previousDate))
                    {
                        diyCell?.colorsView = color
                        selectionType = .middle
                    }
                    else if(CheckDateIsSelected(date: previousDate) == false)
                    {
                        diyCell?.colorsRightView = color
                        selectionType = .singleLeftBorder
                    }
                    else if(prevoiusDateJSON["id"] != currentDateJSON["id"] &&  CheckDateIsSelected(date: previousDate) == true)
                    {
                        diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                        diyCell?.colorsRightView = color
                        selectionType = .bothLeftRightBorder
                    }
                    else if(CheckDateIsSelected(date: nextDate) == false && currentDateJSON["status"].stringValue == BookingStatus.available.rawValue)
                    {
                        diyCell?.colorsLeftView = color
                        selectionType = .singleRightBorder
                    }
                }
            }
            else {
                selectionType = .none
            }
            
            if(CheckDateIsAvailable(strDate: strDate))
            {
                if(CheckDateIsSelected(date: previousDate))
                {
                    diyCell?.colorsLeftView = GetDateColor(date: previousDate)
                    selectionType = .singleRightBorder
                }
            }
            if selectionType == .none
            {
                return
            }
            diyCell?.selectionType = selectionType
        }
        
    }
    
    func returnJSON(strDate:String) -> JSON
    {
        var jsonData = JSON()
        if theModel.arrayReservations.contains(where: { (json) -> Bool in
            if json["date"].stringValue == strDate
            {
                jsonData = json
                return true
            }
            return false
        })
        {
            return jsonData
        }
        return jsonData
    }
    
}
//MARK:- Calendar components delegate

extension CalendarManager : SelectCalendarComponentDelegate
{
    func NextButtonAction() {
        theController.NextMonthAction()
    }
    func PreviousButtonAction() {
        theController.PreviousMonthAction()
    }
    func SelectYearAction(strYear: String) {
        
        theController.SetDropdownSelection(text: strYear)
    }
    
    
}
//MARK:- Helper methods

extension CalendarManager
{
    func CheckDateIsAvailable(strDate:String) -> Bool
    {
        
        if(theModel.arrayAvailable.contains(where: { (json) -> Bool in
            
            if(json["date"].stringValue == strDate)
            {
                return true
            }
            return false
        })){
            return true
        }else{
            return false
        }
    }
    func CheckDateIsSelected(date:Date) -> Bool
    {
        let calendar = theController.GetCurrentCalendar()
        return calendar.selectedDates.contains(date)
    }
    func CheckDateResevered(date:Date) -> Bool
    {
        let calendar = theController.GetCurrentCalendar()
        let checkReserved = calendar.selectedDates.contains(date)
        return checkReserved
    }
    func GetDateColor(date:Date) -> UIColor
    {
        var color = UIColor.clear
        let strDate = theController.formatter.string(from: date)

        if theModel.arrayReservations.contains(where: { (json) -> Bool in
            if(json["date"].stringValue == strDate)
            {
                let index = theModel.arrayReservations.index(of: json)
                color = theModel.arrayDIYColors[index ?? 0]
                
                return true
            }
            return false
        }){
            print("color found")
        }
        return color
    }
}


