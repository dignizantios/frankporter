//
//  DIYCalendarCell.swift
//  FSCalendarSwiftExample
//
//  Created by dingwenchao on 06/11/2016.
//  Copyright © 2016 wenchao. All rights reserved.
//

import Foundation
import FSCalendar

import UIKit

enum SelectionType : Int {
    case none
//    case single   //Remove
    case leftBorder
    case middle
    case rightBorder
    case singleRightBorder
    case singleLeftBorder
    case bothLeftRightBorder
    
//    case singleRight //Remove
//    case singleLeft//Remove
//    case bothLeftRight//Remove
//    case FullRightBorder//Remove

}


class DIYCalendarCell: FSCalendarCell
{
    
//    weak var selectionLayer: CAShapeLayer!
//    weak var selectionLayerLeft: CAShapeLayer!
//    weak var selectionLayerRight: CAShapeLayer!
    
    var selectionLayerView : UIView!
    var selectionWholeLayerView : UIView!
    var selectionLayerLeftView : UIView!
    var selectionLayerRightView : UIView!

//    weak var colors : CGColor!
//    weak var colorsRight : CGColor!
//    weak var colorsLeft : CGColor!
    
    weak var colorsView : UIColor!
    weak var colorsRightView : UIColor!
    weak var colorsLeftView : UIColor!

    let defaultSpace : CGFloat = 20.0
    let defaultHeight : CGFloat = 0
    let defaultY:CGFloat = 0
    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        let selectionLayer = CAShapeLayer()
        selectionLayer.actions = ["hidden": NSNull()]
        self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel!.layer)
        
        ///-----
        
        self.contentView.layer.borderColor = UIColor.black.withAlphaComponent(0.45).cgColor
        self.contentView.layer.borderWidth = 0.5
        
        self.shapeLayer.isHidden = true
        
        let view = UIView(frame: self.bounds)
        view.backgroundColor = .clear
        self.backgroundView = view;
        
        
        ///---------
        self.selectionLayerView = UIView(frame: (self.backgroundView?.bounds)!)
        self.selectionLayerView.backgroundColor = colorsView
        self.backgroundView?.addSubview(selectionLayerView)
        
        //----------
        self.selectionWholeLayerView = UIView(frame: (self.backgroundView?.bounds)!)
        self.selectionWholeLayerView.backgroundColor = colorsView
        self.backgroundView?.addSubview(selectionWholeLayerView)
        
        //---------
        self.selectionLayerLeftView = UIView(frame: CGRect(x: (self.backgroundView?.frame.origin.x)! - ((self.backgroundView?.bounds.width)!/2), y: (self.backgroundView?.frame.origin.y)!, width: (self.backgroundView?.bounds.width)!, height: (self.backgroundView?.bounds.height)!))
        self.selectionLayerLeftView.roundCorners([.topRight,.bottomRight], radius: (self.backgroundView?.bounds.width)!/2)
        self.selectionLayerLeftView.backgroundColor = colorsLeftView
        self.backgroundView?.addSubview(selectionLayerLeftView)
        self.backgroundView?.layer.masksToBounds = true

        
        //---------
        self.selectionLayerRightView = UIView(frame: CGRect(x: view.bounds.width/2, y: view.frame.origin.y, width: view.bounds.width, height: view.bounds.height))
        self.selectionLayerRightView.roundCorners([.topLeft,.bottomLeft], radius: (self.backgroundView?.bounds.width)!/2)
        self.selectionLayerRightView.backgroundColor = colorsRightView
        self.backgroundView?.addSubview(selectionLayerRightView)
        
    }
    ///========new change========
    override func layoutSubviews()
    {
        super.layoutSubviews()
       
        self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
        self.backgroundView?.layer.cornerRadius = 0
        
        selectionLayerView.backgroundColor = colorsView
        selectionLayerLeftView.backgroundColor = colorsLeftView
        selectionLayerRightView.backgroundColor = colorsRightView
        selectionWholeLayerView.backgroundColor = colorsView

        selectionLayerView.isHidden = true
        selectionLayerLeftView.isHidden = true
        selectionLayerRightView.isHidden = true
        selectionWholeLayerView.isHidden = true

        if selectionType == .middle
        {
            selectionLayerView.isHidden = false
        }
        else if selectionType == .leftBorder
        {
            self.selectionWholeLayerView.isHidden = false
            self.selectionWholeLayerView.roundCorners([.topLeft,.bottomLeft], radius: (self.selectionWholeLayerView?.bounds.width)!/2)
            self.selectionWholeLayerView.backgroundColor = colorsView
        }
        else if selectionType == .rightBorder
        {
            self.selectionWholeLayerView.isHidden = false
            self.selectionWholeLayerView.roundCorners([.topRight,.bottomRight], radius: (self.selectionWholeLayerView?.bounds.width)!/2)
            self.selectionWholeLayerView?.backgroundColor = colorsView
        }
        else if selectionType == .singleLeftBorder
        {
            selectionLayerRightView.isHidden = false
        }
        else if selectionType == .singleRightBorder
        {
            selectionLayerLeftView.isHidden = false
        }
        else if selectionType == .bothLeftRightBorder
        {
            selectionLayerLeftView.isHidden = false
            selectionLayerRightView.isHidden = false
        }
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        // Override the build-in appearance configuration
        if self.isPlaceholder {
            self.eventIndicator.isHidden = true
            self.titleLabel.textColor = UIColor.lightGray           
        }
         self.titleLabel.textAlignment = .center
    }
    
}


