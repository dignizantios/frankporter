//
//  CalendarModel.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import SwiftyJSON

class CalendarModel
{
    var arrayYears : [String] = []
    var dictProperty = JSON()
    var arrayReservations : [JSON] = []
    var arrayAvailable : [JSON] = []

    var arrayReservationsList : [JSON] = []
    var arrayDIYColors : [UIColor] = []
    var arrayHalfFillDates : [String] = []

    var dictInvoice = JSON()

    private unowned var theController: CalendarVC
    
    // MARK:- View Lifecycle
    init(theController:CalendarVC) {
        self.theController = theController
    }

    
}
