//
//  InvoiceCell.swift
//  FrankPorter
//
//  Created by Jaydeep on 16/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class InvoiceCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblEarningValue: UILabel!
    @IBOutlet weak var lblStayValue: UILabel!
    @IBOutlet weak var lblAvgNightRateValue: UILabel!
    @IBOutlet weak var lblOccupancyRateValue: UILabel!
    @IBOutlet weak var lblNightsValue: UILabel!
    
    
    @IBOutlet weak var lblEarning: UILabel!
    @IBOutlet weak var lblStay: UILabel!
    @IBOutlet weak var lblAvgNightRate: UILabel!
    @IBOutlet weak var lblOccupancyRate: UILabel!
    @IBOutlet weak var lblNights: UILabel!
    
    //MARK:- viewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        [lblEarning,lblStay,lblAvgNightRate,lblOccupancyRate,lblNights].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 16, fontname: .Boook)
        }
        [lblEarningValue,lblStayValue,lblAvgNightRateValue,lblOccupancyRateValue,lblNightsValue].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 14, fontname: .light)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
