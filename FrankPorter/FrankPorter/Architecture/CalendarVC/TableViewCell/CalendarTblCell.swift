//
//  CalendarTblCell.swift
//  FrankPorter
//
//  Created by YASH on 04/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class CalendarTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var imgWebsiteLogo: UIImageView!
    
    @IBOutlet weak var vwColor: CustomView!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.font = themeFont(size: 16, fontname: .light)
        lblName.textColor = .black
        
        lblDate.font = themeFont(size: 13, fontname: .light)
        lblDate.textColor = UIColor.appThemeLightGreyColor
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
