//
//  CalendarColors.swift
//  FrankPorter
//
//  Created by Khushbu on 12/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation

extension UIColor
{
    
    /*
 
     <color name="bright_pink">#FF007F</color>
     <color name="red">#FF0000</color>
     <color name="orange">#FF7F00</color>
     <color name="yellow">#FFFF00</color>
     <color name="chartreuse">#7FFF00</color>
     <color name="green">#00FF00</color>
     <color name="spring_green">#00FF7F</color>
     <color name="cyan">#00FFFF</color>
     <color name="azure">#007FFF</color>
     <color name="blue">#0000FF</color>
     <color name="violet">#7F00FF</color>
     <color name="magenta">#FF00FF</color>
     <color name="gray">#E6E6E6</color>
     <color name="darkgray">#8D8989</color>
 
     lightRed
     lightPurple
     sky
     lightGreen
     orange
     brown
     lightBlue
     Blue
     Red
     green
     
     let arrayColors : [String] = ["EC7063","BB8FCE","5DADE2","48C9B0","F39c12","935116","AED6F1","1565C0","FF0033","81C784","7F00FF","FF00FF","8D8989"]
     
     
     UIColor.init(red: 52/255, green: 51/255, blue: 40/255, alpha: 1.0),
     UIColor.init(red: 255/255, green: 7/255, blue: 0/255, alpha: 1.0),
     UIColor.init(red: 192/255, green: 80/255, blue: 77/255, alpha: 1.0),
     UIColor.init(red: 79/255, green: 129/255, blue: 189/255, alpha: 1.0)
     ]

    */
    
    static var appThemeLightRed : UIColor { return UIColor.hexStringToUIColor(hex: "EC7063") }
    static var appThemeLightPurple : UIColor { return UIColor.hexStringToUIColor(hex: "BB8FCE") }
    static var appThemeSky : UIColor { return UIColor.hexStringToUIColor(hex: "5DADE2") }
    static var appThemeLightGreen : UIColor { return UIColor.hexStringToUIColor(hex: "48C9B0") }
    static var appThemeOrange : UIColor { return UIColor.hexStringToUIColor(hex: "F39c12") }
    static var appThemeAqua : UIColor { return UIColor.hexStringToUIColor(hex: "09BAC0") }
    static var appThemeLightBlue : UIColor { return UIColor.hexStringToUIColor(hex: "AED6F1") }
    static var appThemeBlue : UIColor { return UIColor.hexStringToUIColor(hex: "1565C0") }
    static var appThemeRed : UIColor { return UIColor.hexStringToUIColor(hex: "FF0033") }
    static var appThemeGreen : UIColor { return UIColor.hexStringToUIColor(hex: "81C784") }
    
    static var appThemeViolet : UIColor { return UIColor.hexStringToUIColor(hex: "957DAD") }
    static var appThemePink : UIColor { return UIColor.hexStringToUIColor(hex: "F694C1") }
    
    
    static var appThemeOwnerDarkGray : UIColor { return UIColor.hexStringToUIColor(hex: "8D8989") }
    static var appThemeUnavailableDarkGray : UIColor { return UIColor.hexStringToUIColor(hex: "E6E6E6") }

    static var appThemeGray : UIColor { return UIColor.hexStringToUIColor(hex: "E6E6E6") }
    static var appThemeDarkGray : UIColor { return UIColor.hexStringToUIColor(hex: "8D8989") }

    
    

}
