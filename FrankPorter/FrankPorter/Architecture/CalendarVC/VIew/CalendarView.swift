//
//  CalendarView.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import FSCalendar


class CalendarView: UIView {

    //MARK: - IBAction
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var tblCalendar: UITableView!
    @IBOutlet weak var vwCalendarCompenents: UIView!
    @IBOutlet weak var progressIndicatorView: UIActivityIndicatorView!

    //MARK: - Variable
    var arrMonthName = [String]()
    let vw = Bundle.main.loadNibNamed("CalendarMonthYearView", owner: self, options: nil)?[0] as? CalendarMonthYearView
    
    // MARK:- UI Methods
    func setupUI(theDelegate: CalendarManager)
    {
        tblCalendar.register(UINib(nibName: "CalendarTblCell", bundle: nil), forCellReuseIdentifier: "CalendarTblCell")
        tblCalendar.register(UINib(nibName: "InvoiceCell", bundle: nil), forCellReuseIdentifier: "InvoiceCell")
        
        calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")

        tblCalendar.tableFooterView = UIView()
        tblCalendar.tableHeaderView = calendar
        setupCaledar(theDelegate: theDelegate)
        
        progressIndicatorView.isHidden = true
        progressIndicatorView.tintColor = UIColor.black
    }
    
    func setTheDelegates(theDelegate: CalendarManager)
    {
        tblCalendar.delegate = theDelegate
        tblCalendar.dataSource = theDelegate
    }

    func setupCaledar(theDelegate: CalendarManager)
    {
        calendar.delegate = theDelegate
        calendar.dataSource = theDelegate
        
        calendar.headerHeight = 0
        calendar.weekdayHeight = 50.0
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.appearance.headerDateFormat = "MMMM"
        calendar.appearance.weekdayTextColor = UIColor.appThemeLightGreyColor
        calendar.appearance.weekdayFont = themeFont(size: 15, fontname: .light)
        calendar.firstWeekday = 2
        calendar.allowsMultipleSelection = true
        
        calendar.appearance.caseOptions = .weekdayUsesUpperCase
        
        calendar.pagingEnabled = true
        calendar.appearance.titleTodayColor = UIColor.black
        
        calendar.appearance.titleFont = themeFont(size: 15, fontname: .light)
        calendar.appearance.titleDefaultColor = UIColor.black
        calendar.appearance.titleSelectionColor = UIColor.black
        
//        calendar.appearance.headerTitleFont = themeFont(size: 15, fontname: .light)
//        calendar.appearance.headerTitleColor = UIColor.appThemeLightGreyColor
        calendar.appearance.headerTitleColor = UIColor.clear
//        self.calendar.appearance.separators = .interRows        
        
       
        
//        calendar.rowHeight = 35
        //TODO:-change
//        calendar.placeholderType = .none
        calendar.appearance.todayColor = .clear
        calendar.clipsToBounds = true // Remove top/bottom line
        calendar.swipeToChooseGesture.isEnabled = true // Swipe-To-Choose
        calendar.scrollEnabled = true
        calendar.placeholderType = .none
//        calendar.appearance.selectionColor = UIColor.clear
    
        vw?.frame = self.vwCalendarCompenents.frame
        vw?.CalendarComponentsDelegate = theDelegate

        self.vwCalendarCompenents.addSubview(vw ?? UIView())
        
        let getMonthArray = Calendar.current
        arrMonthName = getMonthArray.monthSymbols
//        print("arrymonth:\(arrMonthName)")
        SetCalendarMonthValue()
        
        print("Calendar set maximum date value - ",calendar.maximumDate)
        
    }
    func previousMonth()
    {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = -1 // For prev button
        let currentPg = calendar.currentPage
        let currentPage = _calendar.date(byAdding: dateComponents, to: currentPg)

        GetSelectedMonthDataService(date:currentPage ?? Date())

    }
    
    func nextMonth()
    {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = 1 // For next button
        let currentPg = calendar.currentPage
        let currentPage = _calendar.date(byAdding: dateComponents, to:currentPg)
        GetSelectedMonthDataService(date:currentPage ?? Date())

    }
    
    func GetCalendarCurrentMonth() -> Int
    {
        let currentPageDate = calendar.currentPage
        let currentMonth = Calendar.current.component(.month, from: currentPageDate)
        return currentMonth

    }
    func GetCalendarCurrentYear() -> Int
    {
        let currentPageDate = calendar.currentPage
        let currentYear = Calendar.current.component(.year, from: currentPageDate)
        return currentYear
        
    }
    func SetSelectedYear(text:String)
    {
        SetCalendarToSelectedMonthYear(year:Int(text)!,month:GetCalendarCurrentMonth())
    }
    
    func SetCalendarToSelectedMonthYear(year:Int,month:Int)
    {
        let selectedMonth = month < 10 ? "0\(month)" : "\(month)"
        let strStartDate = "01-\(selectedMonth)-\(year)"
        let currentPageDate = calendar.currentPage
        let dateValue = stringTodate(Formatter: "dd-MM-yyyy", strDate: strStartDate)
        let monthDiffrence = currentPageDate.interval(ofComponent: .month, fromDate: dateValue)
        let nextMonth = Calendar.current.date(byAdding: .month, value: -(monthDiffrence) , to: currentPageDate)!
        GetSelectedMonthDataService(date:nextMonth)
    }
    
    func SetCalendarMonthValue()
    {
        vw?.currentMonth = GetCalendarCurrentMonth()
        vw?.SetCurrentMonthYear(strMonth: arrMonthName[GetCalendarCurrentMonth()-1],strYear:"\(GetCalendarCurrentYear())")
    }
    func GetCurrentCalendarStartDate() -> Date
    {
        return calendar.currentPage.startOfMonth() ?? Date()
    }
    func GetCurrentCalendarEndDate() -> Date
    {
        return calendar.currentPage.endOfMonth() ?? Date()
    }
    func GetSelectedMonthDataService(date:Date)
    {
        self.calendar.setCurrentPage(date, animated: true)// calender is object of FSCalendar
    }
}
