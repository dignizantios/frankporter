//
//  CalendarVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import FSCalendar
enum BookingStatus : String
{
    case booked = "booked"
    case available = "available"
    case unavailable = "unavailable"
}


class CalendarVC: UIViewController {

    var strFormatter = "yyyy-MM-dd"
    var strFormatter1 = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX"
    var strFormatter2 = "dd-MMMM-yyyy"
    var strFormatter3 = "yyyy-MM-dd HH:mm:ss"

    var selectedParentVC =  CheckParentVC.calendar
    
    let gregorian = Calendar(identifier: .gregorian)
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX"
        return formatter
    }()
    let formatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMMM-yyyy"
        return formatter
    }()
    let formatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:CalendarView = { [unowned self] in
        return self.view as! CalendarView
        }()
    
    private lazy var theCurrentManager:CalendarManager = { [unowned self] in
        return CalendarManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:CalendarModel = { [unowned self] in
        return CalendarModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedParentVC == .blockoffDates{
            setupNavigationbarwithBackButton(titleText: selectedParentVC.rawValue)
        } else {
            setupNavigationbarwithHomeButton(titleText: "Calendar")
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI(theDelegate: theCurrentManager)
        GetBookingDataService()
        if selectedParentVC == .calendar{
            GetInvoiceService()
        }
    }
    
    //MARK:- Set Data methods
    func SetData(json : JSON)
    {
        theCurrentModel.dictProperty = json
    }
    
    //MARK:- Other methods
    func GetCurrentCalendar() -> FSCalendar
    {
        return theCurrentView.calendar
    }
    func GetCurrentCalendarStartDate() -> Date
    {
        return theCurrentView.calendar.currentPage.startOfMonth() ?? Date()
    }
    func GetCurrentCalendarEndDate() -> Date
    {
        return theCurrentView.calendar.currentPage.endOfMonth() ?? Date()
    }
    
    func SetDropdownSelection(text: String)
    {
        theCurrentView.SetSelectedYear(text: text)
    }
    func NextMonthAction()
    {
        let currentYearName = theCurrentView.vw?.btnYearName.title(for: .normal)
        print("year \(theCurrentView.vw?.btnYearName.title(for: .normal) ?? "")")
        let arrYear = GetYearNames()
        let currentMonth = theCurrentView.GetCalendarCurrentMonth()
        if arrYear.last == currentYearName && currentMonth == 12{
            return
        }
        theCurrentView.nextMonth()
    }
    func PreviousMonthAction()
    {
        theCurrentView.previousMonth()
    }
    func SetSelectedMonth()
    {
        theCurrentView.SetCalendarMonthValue()
    }
    func NavigateToDetails(json:JSON)
    {
        if json.count == 0{
            return
        }
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "GuestDetailsVC") as!  GuestDetailsVC
        obj.SetUpData(json: json)
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK:- Other methods
extension CalendarVC
{
    func ResetCalendarData()
    {
        theCurrentModel.arrayDIYColors = []
        theCurrentModel.arrayReservationsList = []
        theCurrentModel.arrayReservations = []
        theCurrentModel.arrayHalfFillDates = []
        theCurrentModel.arrayAvailable = []
        
    }
    
    func SetUpCalendarData(array: [JSON])
    {
//        ShowProgressLoader()
        ResetCalendarData()
        
        ///
        let monthStartDate = theCurrentView.GetCurrentCalendarStartDate()
        let monthEndDate = theCurrentView.GetCurrentCalendarEndDate()
        
        ///
        let strMonthStartConvertedDate = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter, strDate: DateToString(Formatter: strFormatter3, date: monthStartDate))
        
        let strMonthEndConvertedDate = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter, strDate: DateToString(Formatter: strFormatter3, date: monthEndDate))
        
        ///
        let monthConvertedStartDate = stringTodate(Formatter: strFormatter, strDate: strMonthStartConvertedDate)
        let monthConvertedEndDate = stringTodate(Formatter: strFormatter, strDate: strMonthEndConvertedDate)


        var arrayDatesLocal : [String] = []
        var arrayColorLocal : [UIColor] = []
        var assignColorIndex = -1
        
        array.forEach { (json) in
            
            var dictBook = json
            let strDate = dictBook["date"].stringValue
            
            let DateValue = stringTodate(Formatter: strFormatter, strDate: strDate)
            
            if(dictBook["status"].stringValue == BookingStatus.unavailable.rawValue)
            {
                if(dictBook["ownersReservation"].exists())  //Owner
                {
                    let dictOwnerReservation = dictBook["ownersReservation"]
                    
                    dictBook["id"].stringValue = dictBook["ownerReservationId"].stringValue
                    dictBook["checkIn"].stringValue = dictOwnerReservation["checkIn"].stringValue
                    dictBook["checkOut"].stringValue = dictOwnerReservation["checkOut"].stringValue
                    
                    ///---check in - check out dates manage
                    
                    let strConvertedCheckIn = dictOwnerReservation["checkIn"].stringValue
                    let strConvertedCheckOut = dictOwnerReservation["checkOut"].stringValue
                    
                    let arrayDates = GetAllDatesBetweenStartEnd(strCheckIn:strConvertedCheckIn,strCheckOut:strConvertedCheckOut)
                    
                    
                    arrayDates.forEach({ (reservationDate) in
                        
                        let strReservationDate = DateToString(Formatter: strFormatter, date: reservationDate)
                        
                        dictBook["date"].stringValue = strReservationDate
                        
//                        print("--------unavailable owner-------------")
//                        print("date - ",dictBook["date"].stringValue)
//                        print("status - ",dictBook["status"].stringValue)
                        
                        var color = UIColor()
                        if(reservationDate < monthConvertedStartDate || reservationDate > monthConvertedEndDate)
                        {
                            dictBook["colorIndex"].stringValue = "0"
                            color = UIColor.clear
                        }
                        else{
                            dictBook["colorIndex"].stringValue = "\(GetAllRGBColor().count-2)"
                            color = GetAllRGBColor()[GetAllRGBColor().count-2]
                        }
                        
                        theCurrentView.calendar.select(DateValue, scrollToDate: false)
                        theCurrentModel.arrayReservations.append(dictBook)
                        theCurrentModel.arrayDIYColors.append(color)
                    })
                }
                else{  //Unavailable for booking
                    
                    dictBook["id"].stringValue = dictBook["_id"].stringValue
                    dictBook["colorIndex"].stringValue = "\(GetAllRGBColor().count-1)"
                    let color = GetAllRGBColor()[GetAllRGBColor().count-1]
//
//                    print("-============unavailable============")
//                    print("date - ",dictBook["date"].stringValue)
//                    print("status - ",dictBook["status"].stringValue)
                    theCurrentView.calendar.select(DateValue, scrollToDate: false)
                    theCurrentModel.arrayReservations.append(dictBook)
                    theCurrentModel.arrayDIYColors.append(color)
                }
            }
            else if(dictBook["status"].stringValue == BookingStatus.booked.rawValue)
            {
                let dictReservation = dictBook["reservation"]
                
                dictBook["id"].stringValue = dictBook["reservationId"].stringValue
                dictBook["checkIn"].stringValue = dictReservation["checkIn"].stringValue
                dictBook["checkOut"].stringValue = dictReservation["checkOut"].stringValue
                
                let strConvertedCheckIn = dictReservation["checkIn"].stringValue
                let strConvertedCheckOut = dictReservation["checkOut"].stringValue
                
                let arrayDates = GetAllReservationDatesBetweenStartEnd(strCheckIn:strConvertedCheckIn,strCheckOut:strConvertedCheckOut)
                print("assignColorIndex - ",assignColorIndex)

                if(!arrayDatesLocal.contains(strDate))
                {
                    assignColorIndex = assignColorIndex<GetAllRGBColor().count-2 ? assignColorIndex+1 : 0
                }
                
                arrayDates.forEach({ (reservationDate) in
                    let strReservationDate = DateToString(Formatter: strFormatter, date: reservationDate)
                    if(!arrayDatesLocal.contains(strReservationDate))
                    {
                        arrayDatesLocal.append(strReservationDate)
                    }

                    dictBook["date"].stringValue = strReservationDate
                   
                    var color = UIColor()
                    if(reservationDate < monthConvertedStartDate || reservationDate > monthConvertedEndDate)
                    {
                        dictBook["colorIndex"].stringValue = "0"
                        color = UIColor.clear
                    }
                    else{
                        dictBook["colorIndex"].stringValue = "\(assignColorIndex)"
                        color = GetAllRGBColor()[assignColorIndex]
                    }
                    theCurrentView.calendar.select(DateValue, scrollToDate: false)
                    theCurrentModel.arrayReservations.append(dictBook)
                    theCurrentModel.arrayDIYColors.append(color)
                })
                
            }
                
            else if(dictBook["status"].stringValue == BookingStatus.available.rawValue)
            {
                theCurrentModel.arrayAvailable.append(dictBook)
            }
            //
            
        }
        
//        print("Array arrayReservations final - ",theCurrentModel.arrayReservations)
        print("Array arrayReservations final count - ",theCurrentModel.arrayAvailable.count)

        theCurrentView.calendar.reloadData()
//        HideProgressLoader()
    }
    
    func GetAllDatesBetweenStartEnd(strCheckIn:String,strCheckOut:String) -> [Date]
    {
        let strStartDate = formatter3.date(from: strCheckIn) //For new change
        let strEndDate = formatter3.date(from: strCheckOut)
        let arrayDates = generateDatesArrayBetweenTwoDates(OrinalDtFormater: strFormatter, startDate: strStartDate!, endDate: strEndDate!)
        return arrayDates
    }
    func GetAllReservationDatesBetweenStartEnd(strCheckIn:String,strCheckOut:String) -> [Date]
    {
        let strStartDate = formatter1.date(from: strCheckIn)
        let strEndDate = formatter1.date(from: strCheckOut)
        let arrayDates = generateDatesArrayBetweenTwoDates(OrinalDtFormater: strFormatter1, startDate: strStartDate!, endDate: strEndDate!)
        return arrayDates
    }
    func GetConvertedDatesString(arrayDates : [Date]) -> [String]
    {
        var arrayStrDates : [String] = []
        arrayDates.forEach { (date) in
            let strDate = formatter.string(from: date)
            arrayStrDates.append(strDate)
        }
        return arrayStrDates
    }
    func ShowProgressLoader()
    {
        theCurrentView.progressIndicatorView.isHidden = false
        theCurrentView.progressIndicatorView.startAnimating()
        
    }
    func HideProgressLoader()
    {
        theCurrentView.progressIndicatorView.isHidden = true
        theCurrentView.progressIndicatorView.stopAnimating()
    }
    
}
//MARK:- Block off dates popup
extension CalendarVC
{
    func ShowBlockPopUp(FromDate:Date,isBlock:Bool,dictData:JSON)
    {
        let obj = BlockOffDataSelectVC()
        obj.isSetBlock = isBlock
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        obj.FromDate = FromDate
        obj.ToDate = FromDate.GetDateAfterDays(days: 1)
        obj.notes = dictData["note"].stringValue
        print("dictData - ",dictData)

        if(!isBlock)
        {
            let strCheckIn = dictData["checkIn"].stringValue
            let strCheckOut = dictData["checkOut"].stringValue
            
            let strConvertedCheckIn = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter3, strDate: strCheckIn)
            let strConvertedCheckOut = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter3, strDate: strCheckOut)
            
            obj.FromDate = formatter3.date(from: strConvertedCheckIn) ?? Date()
            obj.ToDate = formatter3.date(from: strConvertedCheckOut) ?? Date().GetDateAfterDays(days: 1)

        }
        
        obj.blockOffHandler = {[weak self] (fromTime,toTime,note,toBackDate,isUpdate)  in
            
            if(!isBlock)
            {
                //Unblock api

                if isUpdate{
                    var dict = JSON()
                    dict["checkIn"].stringValue = fromTime
                    dict["checkOut"].stringValue = toTime
                    dict["listingId"].stringValue = dictData["listingId"].stringValue
                    dict["note"].stringValue = note
                    dict["to"].stringValue = toBackDate
                    dict["apiKey"].stringValue = GuestyAPIKey
                    self?.newUpdateBlockUnblockDatesService(paramJSON: dict, strReservationId: dictData["ownerReservationId"].stringValue)
                } else {
                   self?.newUnblockDate(dict: dictData)
                }
                
            }
            else{
                //block api
                
                var dict = JSON()
                dict["checkIn"].stringValue = fromTime
                dict["checkOut"].stringValue = toTime
                dict["listingId"].stringValue = dictData["listingId"].stringValue
                dict["note"].stringValue = note
                dict["to"].stringValue = toBackDate
                dict["apiKey"].stringValue = GuestyAPIKey
                if isUpdate{
                    self?.newUpdateBlockUnblockDatesService(paramJSON: dict, strReservationId: dictData["ownerReservationId"].stringValue)
                } else {
                    self?.newBlockDatesService(paramJSON: dict)
                }
            }
            
        }
        self.present(obj, animated: true, completion: nil)
        
    }
}
//MARK:- Service
extension CalendarVC
{
    func GetInvoiceService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
//            showLoader()
            
            let URL = BaseServerURL + kGetInvoiceURL
            
            /*
             property_id [Like : 5b70e0fa99b485003e7e7fa6]
             Month [MM] Like (february : 02 december : 12)
             year  [YYYY] Like: 2018,2019
             lang [0 = English]
             */
            
            CommonService().cancelledCurrentRequestifExist(url: URL)
            
            let month = theCurrentView.GetCalendarCurrentMonth()
            let year = theCurrentView.GetCalendarCurrentYear()
            
            let param : [String:String] = ["property_id" : dictSelectedProperty["_id"].stringValue,
                                           "month" : month < 10 ? "0\(month)" : "\(month)",
                "year" : "\(year)",
                "lang" : GetApplicationLanguage()
            ]
            print("param - ",param)
            CommonService().ServerPostService(url: URL, parameters: param,completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["flag"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.theCurrentModel.dictInvoice = data
                    }
                    else{
                        self.theCurrentModel.dictInvoice = JSON()
                    }
                    self.theCurrentView.tblCalendar.reloadData()
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
    
    @objc func GetBookingDataService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
//            ShowProgressLoader()
            //showLoader()
            //listings/{id}/calendar?fields=reservation.guest.fullName reservation.checkIn reservation.checkOut reservation.source status reservation.nightsCount reservation.guestsCount reservation.money.totalPaid reservation.money.currency reservation.money.isFullyPaid reservation.money.ownerRevenue reservation.money.netIncome reservation.money.commissionIncTax
            
//            let URL = BaseURL + "\(kGetBookingDataURL)/" + "\(theCurrentModel.dictProperty["_id"].stringValue)/" + "calendar?fields=reservation.guest.fullName reservation.checkIn reservation.checkOut reservation.source status reservation.nightsCount reservation.guestsCount reservation.money.totalPaid reservation.money.currency reservation.money.isFullyPaid reservation.money.ownerRevenue reservation.money.netIncome reservation.money.commissionIncTax reservation.money.ownerRevenue&from=\(GetFormatedDateValue(date: theCurrentView.GetCurrentCalendarStartDate()))&to=\(GetFormatedDateValue(date: theCurrentView.GetCurrentCalendarEndDate()))"
            
            //TODO:- Changed by JD
            
          //  https:/groot-mailer.production.guesty.com/api/v2/listings/5b703a2b2f6785002c2b8eab/calendar?fields=date+accountId+listingId+status+note+price+currency+listing.prices.currency+reservationId%0A++++reservation+blocks+ownerReservation+ownerReservationId&from=2019-04-01&to=2019-04-30
            
            let URL = "https://groot-mailer.production.guesty.com/api/v2/listings/" + "\(dictSelectedProperty["_id"].stringValue)" + "/calendar?fields=reservation.guest.fullName+reservation.nightsCount+date+accountId+listingId+status+reservation.source+note+price+currency+listing.prices.currency+reservationId%0A++++reservation+blocks+ownerReservation+ownerReservationId&from=\(GetFormatedDateValue(date: theCurrentView.GetCurrentCalendarStartDate()))&to=\(GetFormatedDateValue(date: theCurrentView.GetCurrentCalendarEndDate()))"
            
            
//            CommonService().cancelledCurrentRequestifExist(url: URL)
            CommonService().cancelledAllRequest()

            CommonService().CommonService(url: URL, completion: { (result) in
            
                if let json = result.value
                {
                    
                    let arrayData = json.arrayValue
                    print("response arrayData - ",arrayData)
                    self.SetUpCalendarData(array: arrayData)
                }
                self.stopAnimating()
                self.hideLoader()
//                self.HideProgressLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
    
    func newUnblockDate(dict:JSON){
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
           
            let URL = kNewBaseURLForBlock + "/\(dict["ownerReservationId"].stringValue)"
            
            var paramJSON = JSON()
           
            paramJSON["apiKey"].stringValue = GuestyAPIKey
            
            print("paramJSON - ",paramJSON)
            print("URL - ",URL)
            
            CommonService().POSTJSONService(url: URL,json:paramJSON, completion: { (result) in
                
                if let json = result.value
                {
                    print("Response unblock json - ",json)
                    if(json["_id"].exists())
                    {
                        makeToast(strMessage: "Dates unblocked successfully")
                        self.perform(#selector(self.GetBookingDataService), with: nil, afterDelay: 2.0)
                    }else{
                        makeToast(strMessage: "Something went wrong")
                    }
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
    }
    
    func UnBlockDatesService(dict : JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
           /*
             {
             "status": "canceled",
             "money": {
             "currency": "AED",
             "fareAccommodation": "0",
             "guestTaxes": "0",
             "fareCleaning": "0",
             "hostPayout": "0"
             },
             "integrationId": "5b703c4fe2871501244d2ec2",
             "source": "Direct",
             "listingId": "5b703a2b2f6785002c2b8eab",
             "checkInDateLocalized": "2019-01-14",
             "checkOutDateLocalized": "2019-01-17",
             "guestId": "5c3886cd929ae50019f7d23a"
             }
            */

            let strCheckIn = dict["checkIn"].stringValue
            let strCheckOut = dict["checkOut"].stringValue
            
            let strConvertedCheckIn = stringTodate(OrignalFormatter: strFormatter1, YouWantFormatter: strFormatter, strDate: strCheckIn)
            let strConvertedCheckOut = stringTodate(OrignalFormatter: strFormatter1, YouWantFormatter: strFormatter, strDate: strCheckOut)
            
            let URL = BaseURL + kUnBlockDates + "/\(dict["reservationId"].stringValue)"
            
            var paramJSON = JSON()
            paramJSON["status"].stringValue = BlockUnBlockDates.unblockValue.rawValue
            paramJSON["integrationId"].stringValue = "5b703c4fe2871501244d2ec2"
            paramJSON["source"].stringValue = "Direct"
            paramJSON["listingId"].stringValue = dict["listingId"].stringValue
            paramJSON["checkInDateLocalized"].stringValue = strConvertedCheckIn
            paramJSON["checkOutDateLocalized"].stringValue = strConvertedCheckOut
            paramJSON["guestId"].stringValue = kGuestIdKey
            
            var moneyJSON = JSON()
            moneyJSON["currency"].stringValue = AppCurrency
            moneyJSON["fareAccommodation"].stringValue = "0"
            moneyJSON["guestTaxes"].stringValue = "0"
            moneyJSON["fareCleaning"].stringValue = "0"
            moneyJSON["hostPayout"].stringValue = "0"

            paramJSON["money"] = moneyJSON

            
            print("paramJSON - ",paramJSON)
            print("URL - ",URL)

            CommonService().PUTJSONService(url: URL,json:paramJSON, completion: { (result) in
                
                
                if let json = result.value
                {
//                    let arrayData = json.arrayValue
//                    print("response arrayData - ",arrayData)
//
//                    self.SetUpCalendarData(array: arrayData)
                    print("Response unblock json - ",json)

                    if(json["_id"].exists())
                    {
                        makeToast(strMessage: "Dates unblocked successfully")
                        
                        self.perform(#selector(self.GetBookingDataService), with: nil, afterDelay: 2.0)
                    }else{
                        makeToast(strMessage: "Something went wrong")
                    }
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
    }
    func BlockDatesService(dict : JSON,fromValue:Date,ToValue:Date)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            /*{"status":"confirmed","money":{"currency":"AED","fareAccommodation":"0","guestTaxes":"0","fareCleaning":"0","hostPayout":"0"},"integrationId":"5b703c4fe2871501244d2ec2","source":"Direct","listingId":"5b703a2b2f6785002c2b8eab","checkInDateLocalized":"2019-01-14","checkOutDateLocalized":"2019-01-17","guestId":"5c3886cd929ae50019f7d23a"}
             */
            let strCheckIn = formatter3.string(from: fromValue)
            let strCheckOut = formatter3.string(from: ToValue)
            
            let strConvertedCheckIn = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter, strDate: strCheckIn)
            let strConvertedCheckOut = stringTodate(OrignalFormatter: strFormatter3, YouWantFormatter: strFormatter, strDate: strCheckOut)
            
            
            let URL = BaseURL + kBlockDates
            var paramJSON = JSON()
            paramJSON["status"].stringValue = BlockUnBlockDates.blockValue.rawValue
            paramJSON["integrationId"].stringValue = "5b703c4fe2871501244d2ec2"
            paramJSON["source"].stringValue = "Direct"
            paramJSON["listingId"].stringValue = dict["listingId"].stringValue
            paramJSON["checkInDateLocalized"].stringValue = strConvertedCheckIn
            paramJSON["checkOutDateLocalized"].stringValue = strConvertedCheckOut
            paramJSON["guestId"].stringValue = kGuestIdKey
            
            
            var moneyJSON = JSON()
            moneyJSON["currency"].stringValue = AppCurrency
            moneyJSON["fareAccommodation"].stringValue = "0"
            moneyJSON["guestTaxes"].stringValue = "0"
            moneyJSON["fareCleaning"].stringValue = "0"
            moneyJSON["hostPayout"].stringValue = "0"
            
            paramJSON["money"] = moneyJSON

            
            print("paramJSON - ",paramJSON)
            print("URL - ",URL)
            
            
            CommonService().POSTJSONService(url: URL,json:paramJSON, completion: { (result) in
                
                if let json = result.value
                {
                    if(json["_id"].exists())
                    {
                        makeToast(strMessage: "Dates blocked successfully")
                        self.GetBookingDataService()
                        
                    }else{
                        makeToast(strMessage: "Something went wrong")
                    }
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
        
    }
    
    func newBlockDatesService(paramJSON : JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
//            let URL = BaseURL + kBlockDates
            
            let URL = kNewBaseURLForBlock
            
            print("paramJSON - ",paramJSON)
            print("URL - ",URL)            
            
            CommonService().POSTJSONService(url: URL,json:paramJSON, completion: { (result) in
                
                if let json = result.value
                {
                    if(json["_id"].exists())
                    {
                        makeToast(strMessage: "Dates blocked successfully")
                        self.GetBookingDataService()
                    }else{
                        if json["err"].exists(){
                            makeToast(strMessage: "You can not block date")
                        } else {
                           makeToast(strMessage: "Something went wrong")
                        }
                    }
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
    }
    
    func newUpdateBlockUnblockDatesService(paramJSON : JSON,strReservationId:String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            //            let URL = BaseURL + kBlockDates
            
            let URL = kNewBaseURLForBlock + "/\(strReservationId)"
            
            print("paramJSON - ",paramJSON)
            print("URL - ",URL)
            
            CommonService().PUTJSONService(url: URL,json:paramJSON, completion: { (result) in
                
                if let json = result.value
                {
                    if(json["conversation"]["ownerReservationId"].exists())
                    {
                        makeToast(strMessage: "Dates updated successfully")
                        self.GetBookingDataService()
                        
                    }else{
                        makeToast(strMessage: "Something went wrong")
                    }
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
    }
}
