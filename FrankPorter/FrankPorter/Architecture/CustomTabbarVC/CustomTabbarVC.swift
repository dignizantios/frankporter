//
//  CustomTabbarVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class CustomTabbarVC: UITabBarController,UITabBarControllerDelegate
{
    
    //MARK: - Variable
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.delegate = self
        
        let calendar = mainStoryboard.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        let tabbarOneItem = UITabBarItem(title:"Calendar", image: #imageLiteral(resourceName: "ic_calender_bottom_unselected"), selectedImage: #imageLiteral(resourceName: "ic_calender_bottom_selected"))
        calendar.selectedParentVC = .calendar
        calendar.hidesBottomBarWhenPushed = false
        calendar.SetData(json: dictSelectedProperty)
        isSetMaxCurrentYear = false
        calendar.tabBarItem = tabbarOneItem
        
        /*let invoice = mainStoryboard.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
        let tabbarTwoItem = UITabBarItem(title: "Statistics", image:UIImage(named: "ic_invoice_bottom_unselected"), selectedImage: UIImage(named: "ic_invoice_bottom_selected"))
        invoice.selectedParentVC = .invoice
        isSetMaxCurrentYear = false
        invoice.GetInvoiceData(dict: dictSelectedProperty)
        invoice.tabBarItem = tabbarTwoItem*/
        
        /*let statement = mainStoryboard.instantiateViewController(withIdentifier: "StatementVc") as! StatementVc
        let tabbarTwoItem = UITabBarItem(title: "Statement", image:UIImage(named: "ic_invoice_bottom_unselected"), selectedImage: UIImage(named: "ic_invoice_bottom_selected"))
        statement.SetUpData(json: dictSelectedProperty)
        isSetMaxCurrentYear = true
        statement.hidesBottomBarWhenPushed = false
        statement.tabBarItem = tabbarTwoItem*/
        
        let statement = mainStoryboard.instantiateViewController(withIdentifier: "StatementListVC") as! StatementListVC
        let tabbarTwoItem = UITabBarItem(title: "Statements", image:UIImage(named: "ic_unselected_statements"), selectedImage: UIImage(named: "ic_selected_statements"))
//        statement.SetUpData(json: dictSelectedProperty)
//        isSetMaxCurrentYear = true
        statement.hidesBottomBarWhenPushed = false
        statement.tabBarItem = tabbarTwoItem
        
        let account = mainStoryboard.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
        let tabbarThreeItem = UITabBarItem(title:"Account", image:#imageLiteral(resourceName: "ic_account_bottom_unselected"), selectedImage:#imageLiteral(resourceName: "ic_account_bottom_selected"))
        account.tabBarItem = tabbarThreeItem

        let n1 = UINavigationController(rootViewController:calendar)
        let n2 = UINavigationController(rootViewController:statement)
        let n3 = UINavigationController(rootViewController:account)

        n1.isNavigationBarHidden = false
        n2.isNavigationBarHidden = false
        n3.isNavigationBarHidden = false

        self.viewControllers = [n1,n2,n3]
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor.appThemeDarkGreyColor
        
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowColor = UIColor.appThemeLightGreyColor.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
//        if #available(iOS 10.0, *) {
//            self.tabBar.unselectedItemTintColor = .white
//        } else {
//            // Fallback on earlier versions
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
      
        let navigationController = viewController as? UINavigationController
        navigationController?.popToRootViewController(animated: true)
    }
}

