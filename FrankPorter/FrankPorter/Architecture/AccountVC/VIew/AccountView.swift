//
//  AccountView.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class AccountView: UIView {

    //MARK: - IBAction
    
    @IBOutlet weak var lblMailSupport: UILabel!
    
    @IBOutlet weak var lblBlockOffDates: UILabel!
    
    @IBOutlet weak var lblChangePassword: UILabel!
    
    @IBOutlet weak var lblLogout: UILabel!
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        [lblMailSupport,lblBlockOffDates,lblChangePassword,lblLogout].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .light)
            lbl?.tintColor = UIColor.appThemeLightGreyColor
        }
    }
    
    
    func setTheDelegates(theDelegate: AccountManager)
    {
        
    }

}
