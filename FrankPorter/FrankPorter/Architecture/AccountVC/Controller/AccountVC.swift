//
//  AccountVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON


class AccountVC: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:AccountView = { [unowned self] in
        return self.view as! AccountView
        }()
    
    private lazy var theCurrentManager:AccountManager = { [unowned self] in
        return AccountManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:AccountModel = { [unowned self] in
        return AccountModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        setupNavigationBarwithTitle(titleText: "Account")
        setupNavigationbarwithHomeButton(titleText: "Account")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }

    @IBAction func btnMailSupportAction(_ sender:UIButton){
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "MailSupportVC") as!  MailSupportVC
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnViewStatementTapped(_ sender: UIButton) {
        /*let obj = mainStoryboard.instantiateViewController(withIdentifier: "SelectPropertyVC") as!  SelectPropertyVC
        obj.hidesBottomBarWhenPushed = true

        obj.selectedParentVC = .statement
        self.navigationController?.pushViewController(obj, animated: true)*/
    }
    
    @IBAction func btnBlockOffDateTapped(_ sender: UIButton) {
        
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        isSetMaxCurrentYear = false
        obj.selectedParentVC = .blockoffDates
        obj.SetData(json: dictSelectedProperty)
        self.navigationController?.pushViewController(obj, animated: true)
        
//        let obj = mainStoryboard.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
//        obj.selectedParentVC = .blockoffDates
//        self.navigationController?.pushViewController(obj, animated: true)
        
        /*let obj = mainStoryboard.instantiateViewController(withIdentifier: "SelectPropertyVC") as!  SelectPropertyVC
        obj.selectedParentVC = .blockoffDates
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)*/
    }
    
    @IBAction func btnChangePasswordTapped(_ sender: UIButton) {
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Frankporter", message: "Are you sure to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            self.LogoutService()
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
}
//MARK:- Service
extension AccountVC
{
    func LogoutService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let LogoutURL = BaseLogoutURL + kLogoutURL
            
            let param : [String:String] = [
                                           "apiKey" : GuestyAPIKey
            ]
            print("kLogoutURL - ",LogoutURL)
            
            print("param - ",param)
            
            CommonService().LogoutPostService(url: LogoutURL,parameters: param, completion: { (result) in
                
//                let str = String(decoding: result.data ?? Data(), as: UTF8.self)
                print("Result value - ",result.value)
                if let str = result.value
                {
                    print("response - ",str)
                    
                    if(str == "ok")
                    {
                        let obj = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let navigationController = UINavigationController(rootViewController: obj)
                        appdelgate.window?.rootViewController = navigationController
//                        appdelgate.window?.makeKeyAndVisible()

                        Defaults.removeObject(forKey: kUserDetailsKey)
                        Defaults.synchronize()
                    }
                }
                
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
        
        
    }
}
