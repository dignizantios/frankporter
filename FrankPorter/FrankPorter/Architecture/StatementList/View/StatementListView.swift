//
//  StatementListView.swift
//  FrankPorter
//
//  Created by Jaydeep on 17/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class StatementListView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblStatementList: UITableView!
    
    //MARK:- Private Method
    
    func setupUI()
    {
        tblStatementList.register(UINib(nibName: "StatementListCell", bundle: nil), forCellReuseIdentifier: "StatementListCell")    
        tblStatementList.tableFooterView = UIView()
    }

    func setTheDelegates(theDelegate: StatementListManager)
    {
        tblStatementList.delegate = theDelegate
        tblStatementList.dataSource = theDelegate
    }
    
    func ReloadProperty()
    {
        tblStatementList.reloadData()
    }
}
