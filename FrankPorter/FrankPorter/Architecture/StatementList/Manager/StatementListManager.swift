//
//  StatementListManager.swift
//  FrankPorter
//
//  Created by Jaydeep on 17/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class StatementListManager: NSObject {

    fileprivate unowned var theController: StatementListVC
    fileprivate var theModel:StatementListModel
    
    init(theController:StatementListVC, theModel:StatementListModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}

extension StatementListManager: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(theModel.arrayStatementList.count == 0)
        {
            tableView.backgroundView = theController.backGroundMessageView(strMsg: theModel.strMessage)
            return 0
        }
        tableView.backgroundView = nil
        return theModel.arrayStatementList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "StatementListCell") as! StatementListCell
        let dict = theModel.arrayStatementList[indexPath.row]
        cell.lblMonthName.text = "\(dict["year"].stringValue) \(dict["month_name"].stringValue)"
        cell.lblTotalAmount.text = "AED \(dict["net_amount"].stringValue)"
        if dict["is_read"].stringValue == "0"{
            cell.vwGreen.backgroundColor = UIColor(red: 195/255, green: 224/255, blue: 183/255, alpha: 1.0)
        } else {
            cell.vwGreen.backgroundColor = .white
        }
        cell.selectionStyle = .none   
       
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        theController.redirectToStatement(dictStatement: theModel.arrayStatementList[indexPath.row], tag: indexPath.row)
        
    }
    
}
