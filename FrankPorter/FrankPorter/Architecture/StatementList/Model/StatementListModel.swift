//
//  StatementListModel.swift
//  FrankPorter
//
//  Created by Jaydeep on 17/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class StatementListModel: NSObject {
    
    //MARK:- Variable Declaration
    
    var arrayStatementList : [JSON] = []
    var strMessage = ""
    private unowned var theController: StatementListVC

    // MARK:- View Lifecycle
    init(theController:StatementListVC) {
        self.theController = theController
    }
}
