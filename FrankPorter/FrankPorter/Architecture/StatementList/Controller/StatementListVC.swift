//
//  StatementListVC.swift
//  FrankPorter
//
//  Created by Jaydeep on 17/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire


class StatementListVC: UIViewController {
    
    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:StatementListView = { [unowned self] in
        return self.view as! StatementListView
        }()
    
    private lazy var theCurrentManager:StatementListManager = { [unowned self] in
        return StatementListManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:StatementListModel = { [unowned self] in
        return StatementListModel(theController:self)
        }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.appThemeDarkGreyColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.GetStatementList()
        refreshControl.endRefreshing()
        
    }
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithHomeButton(titleText: "Statements")
    }
    
    func setupUI(){
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
        theCurrentView.setupUI()
        theCurrentView.tblStatementList.addSubview(refreshControl)
        GetStatementList()
    }

}

//MARK:- Other methods
extension StatementListVC
{
    func LoadStatementData(arrayStatementList:[JSON])
    {
        theCurrentModel.arrayStatementList = arrayStatementList
        theCurrentView.ReloadProperty()
    }
    
    func redirectToStatement(dictStatement:JSON,tag:Int){
        let statement = mainStoryboard.instantiateViewController(withIdentifier: "StatementVc") as! StatementVc
        statement.SetUpData(json: dictStatement, tag: tag)
        isSetMaxCurrentYear = true
        statement.hidesBottomBarWhenPushed = true
        statement.handlerReadStatement = {[weak self] selectedTag in
            var dict = self?.theCurrentModel.arrayStatementList[selectedTag]
            dict!["is_read"].stringValue = "1"
            self?.theCurrentModel.arrayStatementList[selectedTag] = dict!
            self?.theCurrentView.ReloadProperty()
        }
        self.navigationController?.pushViewController(statement, animated: true)
    }
}

//MARK:- Service
extension StatementListVC
{
    func GetStatementList()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let kGetBaseStatemetList = kBaseFrankPorterURL + kGetStatementList
            
            let param = ["property_id":dictSelectedProperty["_id"].stringValue,]
            
            print("parma \(param)")
            
            CommonService().ServerPostService(url: kGetBaseStatemetList, parameters: param, completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    self.theCurrentModel.arrayStatementList = []
                    if json["flag"].stringValue == "1"{
                       self.LoadStatementData(arrayStatementList:json["data"].arrayValue)
                    } else {
                        self.LoadStatementData(arrayStatementList:[])
                    }
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
}
