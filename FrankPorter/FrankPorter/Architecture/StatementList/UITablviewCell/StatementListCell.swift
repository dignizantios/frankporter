//
//  StatementListCell.swift
//  FrankPorter
//
//  Created by Jaydeep on 17/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class StatementListCell: UITableViewCell {
    
    //MARK:- Outlet
    
    @IBOutlet weak var vwGreen: UIView!
    @IBOutlet weak var lblMonthName: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblMonthName,lblTotalAmount].forEach { (lbl) in
            lbl?.font = themeFont(size: 17, fontname: .Boook)
            lbl?.textColor = UIColor.black
        }
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
