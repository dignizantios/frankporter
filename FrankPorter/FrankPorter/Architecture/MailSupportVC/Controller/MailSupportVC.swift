//
//  MailSupportVC.swift
//  FrankPorter
//
//  Created by Jaydeep on 12/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class MailSupportVC: UIViewController {
    
    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:MailSupportView = { [unowned self] in
        return self.view as! MailSupportView
        }()
    
    private lazy var theCurrentManager:MailSupportManager = { [unowned self] in
        return MailSupportManager(theController: self, theModel: theCurrentModel, theView: theCurrentView)
        }()
    
    private lazy var theCurrentModel:MailSupportModel = { [unowned self] in
        return MailSupportModel(theController:self)
        }()
    
    //MARK:- view life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        theCurrentView.setupUI(theDelegate: theCurrentManager)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: getCommonString(key: "Mail_support_key"))
    }
    
    //MARK: - IBAction
    
    @IBAction func onBtnPostMailAction(_ sender: UIButton) {
        
        if (theCurrentView.txtSubject.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!{
            makeToast(strMessage: "Please enter subject")
        } else if (theCurrentView.txtViewComment.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!{
            makeToast(strMessage: "Please enter comment")
        } else {
            self.view.endEditing(true)
            postMailSupport()
        }       
    }

}

//MARK:- Service
extension MailSupportVC
{
    func postMailSupport()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let kSupportURL = kBaseFrankPorterURL + kSendMailSupport
            let param : [String:String] = ["subject" : self.theCurrentView.txtSubject.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                                           "message" : self.theCurrentView.txtViewComment.text.trimmingCharacters(in: .whitespaces) ,
                                           "full_name":getUserDetail("fullName"),
                                           "owner_email_id":getUserDetail("email")
                                           
            ]
            
            print("param \(param)")
            CommonService().ServerPostService(url: kSupportURL, parameters: param, completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if json["flag"].stringValue == "1"{
                        makeToast(strMessage: json["data"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        makeToast(strMessage: json["data"].stringValue)
                    }
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
    }
}
