//
//  MailSupportView.swift
//  FrankPorter
//
//  Created by Jaydeep on 12/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MailSupportView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblSubjectTitle: UILabel!
    @IBOutlet weak var txtSubject: CustomTextField!
    @IBOutlet weak var lblCommentTitle: UILabel!
    @IBOutlet weak var txtViewComment: CustomTextview!
    @IBOutlet weak var btnSubmitOutlet: CustomButton!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
    //MARK:- Setup UI
    
    func setupUI(theDelegate:MailSupportManager)  {
        
        [lblSubjectTitle,lblCommentTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .light)
            lbl?.textColor = UIColor.black
        }
        
        [lblPlaceholder].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .light)
            lbl?.textColor = UIColor.appThemeLightGreyColor
        }
        
        [txtSubject].forEach { (txtfld) in
            txtfld?.layer.borderColor = (UIColor.appThemeLightGreyColor).cgColor
            txtfld?.layer.borderWidth = 01
            txtfld?.leftPaddingView = 15
            txtfld?.font = themeFont(size: 17, fontname: .light)
            txtfld?.delegate = theDelegate
        }
        
        [txtViewComment].forEach { (txtview) in
            txtview?.font = themeFont(size: 17, fontname: .light)
            txtview?.textContainerInset = UIEdgeInsets(top: 8, left: 13, bottom: 6, right: 13)
            txtview?.delegate = theDelegate
            txtview?.font = themeFont(size: 17, fontname: .light)
        }
        
        [btnSubmitOutlet].forEach { (btn) in
            btn?.backgroundColor = UIColor.appThemeRed
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .light)
        }
        
    }
    

}
