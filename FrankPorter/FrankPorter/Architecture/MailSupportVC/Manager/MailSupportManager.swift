//
//  MailSupportManager.swift
//  FrankPorter
//
//  Created by Jaydeep on 12/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MailSupportManager: NSObject {
    
    fileprivate unowned var theController: MailSupportVC
    fileprivate var theModel:MailSupportModel
    fileprivate var theView:MailSupportView
    
    init(theController:MailSupportVC, theModel:MailSupportModel,theView:MailSupportView) {
        self.theController = theController
        self.theModel = theModel
        self.theView = theView
    }

}

//MARK:- Textfield delegate
extension MailSupportManager : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""{
            theView.lblPlaceholder.isHidden = false
        } else {
            theView.lblPlaceholder.isHidden = true
        }
    }
}

//MARK:- Textfield delegate
extension MailSupportManager : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {        
        theController.view.endEditing(true)
        return true
    }
}
