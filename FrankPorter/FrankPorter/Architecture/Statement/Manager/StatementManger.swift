//
//  StatementManger.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class StatementManger: NSObject {

    fileprivate unowned var theController: StatementVc
    fileprivate var theModel:StatementModel
    
    init(theController:StatementVc, theModel:StatementModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}

extension StatementManger: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(theModel.arrayStatement.count == 0)
        {
            tableView.backgroundView = theController.backGroundMessageView(strMsg: theModel.strMessage)
            return 0
        }
        tableView.backgroundView = nil
        return theModel.arrayStatement.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
 
        let dict = theModel.arrayStatement[section]
        let vw = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?[0] as? HeaderView

        vw?.lblTitleName.text = dict["titleLabel"].stringValue
        
        
        if(dict["isDefault"].stringValue == "0")
        {
            if(dict["isShowCount"].stringValue == "1")
            {
                vw?.lblCount.text = "\(theModel.arrayStatement[section]["dataArray"].arrayValue.count)"
                vw?.lblCount.isHidden = false
            }else{
                vw?.lblCount.isHidden = true
            }
        }
        else{
            
            if(dict["isDeduct"].stringValue == "0")
            {
                vw?.lblCount.text = "\(AppCurrency) " +  "\(GetFormatedPrice(strPrice: theModel.arrayStatement[section]["TotalValue"].floatValue) )"
                vw?.vwBottom.isHidden = true
            }else{
                vw?.lblCount.text = "-\(AppCurrency) " + "\(GetFormatedPrice(strPrice: theModel.arrayStatement[section]["TotalValue"].floatValue))"
                vw?.vwBottom.isHidden = false
            }
        }
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return theModel.arrayStatement[section]["dataArray"].arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "StatementTableCell") as! StatementTableCell
        
        let dict = theModel.arrayStatement[indexPath.section]
        let array = dict["dataArray"].arrayValue
        let dictData = array[indexPath.row]
        //
        let status = dictData["type"].stringValue
        switch(status)
        {
            case AmountType.reservations.rawValue :
                cell.lblName.text = dictData["guest_name"].stringValue
                cell.lblPrice.text = "\(AppCurrency) " + GetFormatedPrice(strPrice: dictData["gross_revenue"].floatValue)
            case AmountType.expense.rawValue :
                cell.lblName.text = dictData["expense_name"].stringValue
                cell.lblPrice.text = "-\(AppCurrency) " +  GetFormatedPrice(strPrice: dictData["amount"].floatValue)
            case AmountType.refund_data.rawValue :
                cell.lblName.text = dictData["refund_name"].stringValue
                cell.lblPrice.text = "+\(AppCurrency) " +  GetFormatedPrice(strPrice: dictData["amount"].floatValue)
            default:
                cell.lblName.text = ""
                cell.lblPrice.text = ""
        }
        
        
        
        /*if(dictData["isReservation"].stringValue == "1")
        {
            cell.lblName.text = dictData["guest_name"].stringValue
            cell.lblPrice.text = "AED - " + dictData["gross_revenue"].stringValue
            
        }
        else{
            cell.lblName.text = dictData["expense_name"].stringValue
            cell.lblPrice.text = "AED - " + dictData["amount"].stringValue

        }*/
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//MARK:- Calendar components delegate

extension StatementManger : SelectCalendarComponentDelegate
{
    func NextButtonAction()
    {
    
        if(theModel.Month == GetCurrentMonth()-1 && theModel.Year == GetCurrentYear())
        {
            makeToast(strMessage: "Cannot view future statement ")
            return
        }
        if(theModel.Month < 12)
        {
            theModel.Month = theModel.Month + 1
        }
        
        else{
            theModel.Month = 1
            theModel.Year = theModel.Year + 1
        }
        theController.NextMonthAction()

    }
    func PreviousButtonAction()
    {
        if(theModel.Month == 1)
        {
            theModel.Month = 12
            theModel.Year = theModel.Year - 1

        }else{
            theModel.Month = theModel.Month - 1
        }
        theController.PreviousMonthAction()
    }
    func SelectYearAction(strYear: String) {
        
        theModel.Year = Int(strYear) ?? GetCurrentYear()
        theController.SetDropdownSelection(text: strYear)
    }
    
    
    
}
