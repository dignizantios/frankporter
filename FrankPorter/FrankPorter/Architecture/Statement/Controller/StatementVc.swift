//
//  StatementVc.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
enum AmountType : String
{
    case reservations = "reservations"
    case expense = "expenses_data"
    case refund_data = "refunds_data"
}

class StatementVc: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:StatementView = { [unowned self] in
        return self.view as! StatementView
        }()
    
    private lazy var theCurrentManager:StatementManger = { [unowned self] in
        return StatementManger(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:StatementModel = { [unowned self] in
        return StatementModel(theController:self)
        }()
    
    var handlerReadStatement: (_ tag : Int) -> Void = {_ in}
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        setupNavigationbarwithHomeButton(titleText: "Statement")       
        setupNavigationbarwithBackButton(titleText: "Statement \(self.theCurrentModel.dictProperty["month"].stringValue) \(self.theCurrentModel.dictProperty["year"].stringValue)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
        
        theCurrentView.SetSelectedMonthYear(strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")
        GetStatementService()
    }

}
//MARK:- SetUpData

extension StatementVc
{
    func SetDropdownSelection(text: String)
    {
        //theCurrentView.SetSelectedYear(text: text)
//        theCurrentModel.strYear = text
//        theCurrentView.SetSelectedMonthYear(strMonth: "\(theCurrentModel.Month)", strYear: "\(theCurrentModel.Year)")
        
        GetStatementService()
    }
    func NextMonthAction()
    {
        //theCurrentView.nextMonth()
//        theCurrentModel.strYear = text
        
        theCurrentView.SetSelectedMonthYear(strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")
        GetStatementService()
    }
    func PreviousMonthAction()
    {
        //theCurrentView.previousMonth()
//        theCurrentModel.strYear = text
        
        theCurrentView.SetSelectedMonthYear(strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")
        GetStatementService()
    }

    
}
//MARK:- SetUpData
extension StatementVc
{
    func SetUpData(json:JSON,tag:Int)
    {
        theCurrentModel.dictProperty = json
        theCurrentModel.int_selectedTag = tag
//        GetStatementService()
    }
    func ResetStatementData()
    {
        self.theCurrentModel.arrayStatement = []
        self.theCurrentView.tblStatement.reloadData()
        self.theCurrentView.lblSubTotalValue.text = ""
        self.theCurrentView.lblSubTotal.isHidden = true
        
    }
    func setUpServiceData(data:JSON)
    {
        
        theCurrentView.SetData(json: data,strMonth: "\(GetAllMonthsNames()[theCurrentModel.Month-1])", strYear: "\(theCurrentModel.Year)")
        
        var arrayReservations : [JSON] = []
        var arrayExpense : [JSON] = []
        var arrayAddition : [JSON] = []

        
        var json = JSON()
        var Total : Float = 0.0
        data["reservations"].arrayValue.forEach
            { (dict) in
            var dictReservation = dict
            dictReservation["type"].stringValue = AmountType.reservations.rawValue
            Total = Total + dictReservation["gross_revenue"].floatValue
            arrayReservations.append(dictReservation)
        }
        
        if(data["expenses_data"].arrayValue.count > 0)
        {
            data["expenses_data"].arrayValue.forEach { (dict) in
                
                var dictExpense = dict
                dictExpense["type"].stringValue = AmountType.expense.rawValue
                arrayExpense.append(dictExpense)
            }
        }
        if(data["refunds_data"].arrayValue.count > 0)
        {
            data["refunds_data"].arrayValue.forEach { (dict) in
                
                var dictAddition = dict
                dictAddition["type"].stringValue = AmountType.refund_data.rawValue
                arrayAddition.append(dictAddition)
            }
        }
        
        theCurrentModel.arrayStatement = []
        if(arrayReservations.count>0)
        {
            json = JSON()
            json["dataArray"] = JSON(arrayReservations)
            json["titleLabel"].stringValue = "Total Reservation"
            json["isDefault"] = "0"
            json["isShowCount"] = "1"

            theCurrentModel.arrayStatement.append(json)
            
            
            json=JSON()
            json["titleLabel"] = "Total"
            json["dataArray"] = []
            json["isDefault"] = "1"
            
            json["isDeduct"] = "0"
            json["TotalValue"] = JSON(Total)

            theCurrentModel.arrayStatement.append(json)

            json=JSON()
            json["titleLabel"] = "Management Fee"
            json["TotalValue"] = data["mgm_fee"]
            json["isDefault"] = "1"
            json["dataArray"] = []
            json["isDeduct"] = "1"

            theCurrentModel.arrayStatement.append(json)

        }
        
        if(arrayExpense.count>0)
        {
            json = JSON()
            json["dataArray"] = JSON(arrayExpense)
            json["titleLabel"].stringValue = "Expenses"
            json["isShowCount"] = "0"
            json["isDefault"] = "0"

            theCurrentModel.arrayStatement.append(json)
        }
        
        if(arrayAddition.count>0)
        {
            json = JSON()
            json["dataArray"] = JSON(arrayAddition)
            json["titleLabel"].stringValue = "Additions"
            json["isShowCount"] = "0"
            json["isDefault"] = "0"

            theCurrentModel.arrayStatement.append(json)
        }
        
        print("Final array - ",theCurrentModel.arrayStatement)
        theCurrentView.tblStatement.reloadData()
        
    }
}


//MARK:- Service
extension StatementVc
{
    func GetStatementService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let URL = BaseServerURL + kGetStatementURL
            
            /*
             property_id [Like : 5b70e0fa99b485003e7e7fa6]
             Month [MM] Like (february : 02 december : 12)
             year  [YYYY] Like: 2018,2019
             lang [0 = English]
            */
            
            /*let param : [String:String] = ["property_id" : theCurrentModel.dictProperty["_id"].stringValue,
                                           "month" : theCurrentModel.Month < 10 ? "0\(theCurrentModel.Month)" : "\(theCurrentModel.Month)",
                                           "year" : "\(theCurrentModel.Year)",
                                           "lang" : GetApplicationLanguage()
                        ]*/
            let param : [String:String] = ["property_id" : dictSelectedProperty["_id"].stringValue,
                                           "month" : theCurrentModel.dictProperty["month"].stringValue,
                                           "year" : theCurrentModel.dictProperty["year"].stringValue,
                                           "lang" : GetApplicationLanguage()
            ]
            print("param - ",param)
            CommonService().ServerPostService(url: URL, parameters: param,completion: { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)

                    if(json["flag"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.setUpServiceData(data:data)
                        self.handlerReadStatement(self.theCurrentModel.int_selectedTag)
                        self.theCurrentView.lblMonthFullNameinTableHeader.isHidden = false
                        self.theCurrentView.lblMonthFullNameinTableHeader.text = "Statement \(self.theCurrentModel.dictProperty["month"].stringValue) \(self.theCurrentModel.dictProperty["year"].stringValue)"
//                        self.theCurrentView.lblMonthFullNameinTableHeader.text = "Statement \(GetAllMonthsNames()[self.theCurrentModel.Month-1]) \(self.theCurrentModel.Year)"
                    }
                    else{
                        self.theCurrentModel.strMessage =  json["msg"].stringValue

                        self.ResetStatementData()
                        makeToast(strMessage: json["msg"].stringValue)
                        self.theCurrentView.lblMonthFullNameinTableHeader.isHidden = true
                    }

                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
    }
    
    
}


