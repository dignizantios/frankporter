//
//  StatementView.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
class StatementView: UIView {

    //MARK: - Outlet
    
//    @IBOutlet weak var lblMonthName: UILabel!
    
    @IBOutlet weak var vwTableHeader: UIView!
    
    @IBOutlet weak var lblMonthFullNameinTableHeader: UILabel!
    
    @IBOutlet weak var tblStatement: UITableView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblSubTotalValue: UILabel!
    @IBOutlet weak var vwCalendarCompenents: UIView!

    
    var vw = Bundle.main.loadNibNamed("CalendarMonthYearView", owner: self, options: nil)?[0] as? CalendarMonthYearView

    //MARK: - Setup UI
    
    func setupUI()
    {
        lblMonthFullNameinTableHeader.font = themeFont(size: 19, fontname: .light)
        lblMonthFullNameinTableHeader.textColor = .black
        
        lblSubTotal.font = themeFont(size: 16, fontname: .Boook)
        lblSubTotalValue.font = themeFont(size: 16, fontname: .Boook)
        
        lblSubTotal.text = "Total"
        lblSubTotalValue.text = ""
        lblSubTotal.isHidden = true
        
        lblMonthFullNameinTableHeader.isHidden = true
        
        tblStatement.register(UINib(nibName: "StatementTableCell", bundle: nil), forCellReuseIdentifier: "StatementTableCell")
        tblStatement.tableFooterView = UIView()
//        tblStatement.tableHeaderView = vwTableHeader
        
        vw?.frame = self.vwCalendarCompenents.frame
        self.vwCalendarCompenents.addSubview(vw ?? UIView())
        
        
    }
    
    
    func setTheDelegates(theDelegate: StatementManger)
    {
        tblStatement.delegate = theDelegate
        tblStatement.dataSource = theDelegate
        vw?.CalendarComponentsDelegate = theDelegate

    }

    func SetSelectedMonthYear(strMonth:String,strYear:String)
    {
        vw?.SetCurrentMonthYear(strMonth: strMonth,strYear:strYear)

    }
    func SetData(json : JSON,strMonth:String,strYear:String)
    {
//        lblMonthFullNameinTableHeader.text = "Statement \(strMonth) \(strYear)"
        lblSubTotalValue.text = "\(AppCurrency) \(GetFormatedPrice(strPrice: json["net_amount"].floatValue))"
        
        lblSubTotal.isHidden = false
    }
    
}



