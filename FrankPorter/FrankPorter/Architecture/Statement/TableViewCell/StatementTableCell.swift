//
//  StatementTableCell.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class StatementTableCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
