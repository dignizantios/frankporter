//
//  HeaderView.swift
//  Telacoach
//
//  Created by Jaydeep on 16/07/18.
//  Copyright © 2018 om. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitleName: UILabel!
    
    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var vwBottom: UIView!
    //MARK:- ViewLifeCycle
    
    override func awakeFromNib() {

        [lblTitleName,lblCount].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 18, fontname: .Boook)
        }
        
    }

}
