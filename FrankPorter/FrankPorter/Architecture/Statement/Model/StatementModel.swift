//
//  StatementModel.swift
//  FrankPorter
//
//  Created by YASH on 29/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import SwiftyJSON


class StatementModel {
    
    private unowned var theController: StatementVc
    
    var dictProperty = JSON()
    var Year = GetCurrentMonth() > 1 ? GetCurrentYear() : GetCurrentYear()-1
    var Month = GetCurrentMonth() > 1 ? GetCurrentMonth()-1 : 12

    var arrayStatement : [JSON] = []
    var strMessage = ""
    var int_selectedTag = Int()

    // MARK:- View Lifecycle
    init(theController:StatementVc) {
        self.theController = theController
    }

    
}
