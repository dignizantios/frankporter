//
//  ForgotPasswordVC.swift
//  FrankPorter
//
//  Created by YASH on 27/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ForgotPasswordVC: UIViewController {

    // MARK: - Variables | Properties
    fileprivate lazy var theCurrentView:ForgotPasswordView = { [unowned self] in
        return self.view as! ForgotPasswordView
        }()
    
    private lazy var theCurrentManager:ForgotPasswordManager = { [unowned self] in
        return ForgotPasswordManager(theController: self, theModel: theCurrentModel)
        }()
    
    private lazy var theCurrentModel:ForgotPasswordModel = { [unowned self] in
        return ForgotPasswordModel(theController:self)
        }()
    
    //MAR: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_cancel_black"), style: .plain, target: self, action: #selector(backPopAction))
        rightButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.hidesBackButton = true
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = getCommonString(key: "Forgot_password_key")
        HeaderView.textColor = UIColor.black
        HeaderView.font = themeFont(size: 17, fontname: .light)
        
        self.navigationItem.titleView = HeaderView

        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       
    }
    
    func setupManager()
    {
        self.setupUI()
        theCurrentView.setTheDelegates(theDelegate: theCurrentManager)
    }
    
    func setupUI()
    {
        theCurrentView.setupUI()
    }

}
//MARK:- Other methods

extension ForgotPasswordVC
{
    @objc func backPopAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Button Action
extension ForgotPasswordVC
{
    @IBAction func btnForgotPasswordAction(_ sender : UIButton)
    {
        if((theCurrentView.txtEmail.text?.isEmpty)!)
        {
            makeToast(strMessage: "Please enter email address")
        }
        else if(!isValidEmail(emailAddressString: theCurrentView.txtEmail.text!))
        {
            makeToast(strMessage: "Please enter valid email address")
        }
        else{
            ForgotPasswordService()
        }
    }
}

//MARK:- Service
extension ForgotPasswordVC
{
    func ForgotPasswordService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let URL = BaseURL + kForgotPasswordURL
            
            //{username=haresh@dignizant.com, apiKey=KdDnXIZcVp0HBF2pzzVDHvNqE0CwxVnp}

            
            let param : [String:String] = ["username" : theCurrentView.txtEmail.text ?? "",
                                           "apiKey" : GuestyAPIKey
                
            ]
            print("param - ",param)
            CommonService().ForgotPasswordPostService(url: URL, parameters: param,completion: { (result) in
                
                if let str = result.value
                {
                    print("response - ",str)
                    
                    if(str.lowercased() == "ok")
                    {
                        makeToast(strMessage: "Check your email and follow the instructions provided to reset your password")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        makeToast(strMessage: str)

                    }
                    
                }
                self.stopAnimating()
                self.hideLoader()
            })
        }
        else{
            makeToast(strMessage: getCommonString(key: "No_internet_key"))
        }
        
    }
}
