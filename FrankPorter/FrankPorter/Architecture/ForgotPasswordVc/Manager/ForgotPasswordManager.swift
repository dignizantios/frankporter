//
//  ForgotPasswordManager.swift
//  FrankPorter
//
//  Created by YASH on 27/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class ForgotPasswordManager: NSObject {

    fileprivate unowned var theController: ForgotPasswordVC
    fileprivate var theModel:ForgotPasswordModel
    
    init(theController:ForgotPasswordVC, theModel:ForgotPasswordModel) {
        self.theController = theController
        self.theModel = theModel
    }
    
}
