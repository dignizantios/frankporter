
//
//  ForgotPasswordView.swift
//  FrankPorter
//
//  Created by YASH on 27/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIView {

    //MARK: - outlet
    
    @IBOutlet weak var lblForgotPassword: UIButton!
    
    @IBOutlet weak var lblInstrucation: UILabel!
    
    @IBOutlet weak var txtEmail: CustomTextField!
    
    @IBOutlet weak var btnResetPassword: CustomButton!
    
    // MARK:- UI Methods
    func setupUI()
    {
        
        txtEmail.backgroundColor = UIColor.appThemeBackgroundLightColor
        txtEmail.layer.borderColor = (UIColor.appThemeLightGreyColor).cgColor
        txtEmail.layer.borderWidth = 0.5
        txtEmail.leftPaddingView = 18
        txtEmail.font = themeFont(size: 17, fontname: .light)
        
        btnResetPassword.titleLabel?.font = themeFont(size: 17, fontname: .light)
        
    }
    
    
    func setTheDelegates(theDelegate: ForgotPasswordManager)
    {
        
    }

}
