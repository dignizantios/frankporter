//
//  Service.swift
//  Hand2Home
//
//  Created by YASH on 02/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON



struct CommonService
{
    func cancelledAllRequest() {
        Alamofire.SessionManager.default.session.getAllTasks { (urlSessionTasks) in
            urlSessionTasks.forEach({$0.cancel()})
        }
    }
    
    func cancelledCurrentRequestifExist(url:String) {
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadDataTask, downloadDataTask) in
            if let index = sessionDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                sessionDataTask[index].cancel()
            }
            if let index = uploadDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                uploadDataTask[index].cancel()
            }
            if let index = downloadDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                downloadDataTask[index].cancel()
            }
            
        }
        
    }
    
    func AuthenticateService(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")

        print("param - ",param)

        print("url - ",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON(completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)

                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })
    }
    func CommonService(url:String,completion:@escaping(Result<JSON>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")

        print("url - ",url.encodedURLString())
        print("GetToken - ",GetToken())

        let headers: HTTPHeaders = [
            "Authorization": "\(kUserName) \(GetToken())",
            "Content-Type":"application/json"
        ]
       
        Alamofire.request(url.encodedURLString(), method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: headers).responseSwiftyJSON(completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
//                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })
    }
    
    func PostService(url:String, parameters: [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")

        print("url - ",url)
        print("Token - ",GetToken())

        let headers: HTTPHeaders = [
            "Authorization": "\(kUserName) \(GetToken())"
        ]
        
        Alamofire.request(url.encodedURLString(), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseSwiftyJSON(completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })
    }
    func ForgotPasswordPostService(url:String, parameters: [String:String],completion:@escaping(Result<String>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")
        
        print("url - ",url)
        
        Alamofire.request(url.encodedURLString(), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseString(completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })
    }
    func LogoutPostService(url:String, parameters: [String:String],completion:@escaping(Result<String>)-> Void)
    {
        print("----------------------------------------------------------------------------------------------")
        
        print("url - ",url)
        print("Token - ",GetToken())
        
        let headers: HTTPHeaders = [
            "Authorization": "\(kUserName) \(GetToken())"
        ]
        
        Alamofire.request(url.encodedURLString(), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseString(completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })

        
    }
    func ServerPostService(url:String, parameters: [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")
        
        print("url - ",url)
        let credentialData = "\(kServerUserName):\(kServerPassword)".data(using: .utf8)
        guard let cred = credentialData else { return }
        let base64Credentials = cred.base64EncodedData(options: [])
        guard let base64Date = Data(base64Encoded: base64Credentials) else { return  }
       
        let headers: HTTPHeaders = [
           "Authorization": "Basic \(base64Date.base64EncodedString())"
        ]
        
        
        Alamofire.request(url.encodedURLString(), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).authenticate(user: kServerUserName, password: kServerPassword).responseSwiftyJSON(completionHandler:
            {
                print("API result - ",$0.result)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
//                    let statusCode = $0.response?.statusCode
//                    makeToast(strMessage: getCommonString(key: "Server_not_responding_key"))
                    completion($0.result)
                    
                }else
                {

//                    makeToast(strMessage: getCommonString(key: "Server_not_responding_key"))
                    completion($0.result)
                }
        })
    }
    func PUTJSONService(url:String, json: JSON,completion:@escaping(Result<JSON>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")
        
        print("url - ",url)
        
        let headers: HTTPHeaders = [
            "Authorization": "\(kUserName) \(GetToken())",
            "Content-Type":"application/json"
        ]
        
        
//        Alamofire.request(.POST, url.encodedURLString(), body: "myBodyString")
        let strJSON = json.rawString() ?? ""
        
    
        Alamofire.request(url.encodedURLString(), method: .put, parameters: nil, encoding: SingleValueJSONEncoding.init(string: strJSON), headers: headers).responseSwiftyJSON (completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })
    }
    func POSTJSONService(url:String, json: JSON,completion:@escaping(Result<JSON>)-> ())
    {
        print("----------------------------------------------------------------------------------------------")
        
        print("url - ",url)
        
        let headers: HTTPHeaders = [
            "Authorization": "\(kUserName) \(GetToken())",
            "Content-Type":"application/json"
        ]
        
        
        //        Alamofire.request(.POST, url.encodedURLString(), body: "myBodyString")
        let strJSON = json.rawString() ?? ""
        
        print("encoding \(SingleValueJSONEncoding.init(string: strJSON))")
        
        Alamofire.request(url.encodedURLString(), method: .post, parameters: nil, encoding: SingleValueJSONEncoding.init(string: strJSON), headers: headers).responseSwiftyJSON (completionHandler:
            {
                print("API result - ",$0.result)
                
                let statusCode = $0.response?.statusCode
                let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                
                print("API result str - ",str)
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    print("Status code - ",statusCode)
                    if(statusCode == 200)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: str)
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: str)
                    completion($0.result)
                }
        })
    }
}

struct SingleValueJSONEncoding: ParameterEncoding {
    private let myString: String
    
    init(string: String) {
        self.myString = string
    }
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = urlRequest.urlRequest
        
        let data = myString.data(using: .utf8)!
        
        let loginString = String(format: "%@:%@", kBasicAuthUserName, kBasicAuthPassword)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        if urlRequest?.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest?.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            urlRequest?.setValue("Basic MTQxZDA2YTkwNzBmZmIzNzlmZmJmMmJiZDAzZTU1ZTE6YWZjYzBiZjY4NTM2MzFjMDFiODU2ZWJhMGVjMGM4NjI=", forHTTPHeaderField: "Authorization")
            urlRequest?.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")


        }
        
        urlRequest?.httpBody = data
        return urlRequest!
    }
}
