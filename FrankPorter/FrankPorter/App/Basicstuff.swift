//
//  Basicstuff.swift
//  SimpReg
//
//  Created by Jaydeep on 06/07/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import Foundation
import MaterialComponents
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import NVActivityIndicatorView

enum BlockUnBlockDates : String
{
    case blockValue = "confirmed"
    case unblockValue = "canceled"
}
enum Languages : String
{
    case English = "English"
}

var ApplicationLanguage = Languages.English

let Defaults = UserDefaults.standard
let appdelgate = UIApplication.shared.delegate as! AppDelegate

//Mapping
let mapping:StringMapping = StringMapping.shared()
let StringFilePath = Bundle.main.path(forResource: "Language", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let kGuestIdKey = "5c3886cd929ae50019f7d23a"
//
let device_type = "1"
let AppCurrency = "AED"

//MARK: - SetLanguage

func setLangauge(language:String)
{
    //TODO:- Change language according to application language in stringmapping file
    
    Defaults.set(ApplicationLanguage.rawValue, forKey: "SelectedLanguage")
    Defaults.synchronize()
    StringMapping.shared().setLanguage()
}

func GetApplicationLanguage() -> String
{
    if(ApplicationLanguage == .English)
    {
        return "0"
    }
    else{
        return "1"
    }
}
//MARK:-
func GetFormatedPrice(strPrice:Float) -> String
{
    let price = String(format: "%.2f",strPrice)
    return price
}

//MARK: - Font family


func printFonts()
{
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName )
        print("Font Names = [\(names)]")
    }
}



func getCommonString(key:String) -> String
{
    let dictLanguage = dictStrings?.object(forKey: ApplicationLanguage.rawValue) as? NSDictionary
    return dictLanguage?.object(forKey: key) as? String ?? ""
}

//MARK:- Date Formatter

func stringTodate(Formatter:String,strDate:String) -> Date
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Formatter
    //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
    let FinalDate = dateFormatter.date(from: strDate)!
    return FinalDate
}

func DateToString(Formatter:String,date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Formatter
    //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
    let FinalDate:String = dateFormatter.string(from: date)
    return FinalDate
}

func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = OrignalFormatter
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = YouWantFormatter
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
    
}

func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = OrignalFormatter
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = YouWantFormatter
    return dateFormatterPrint.date(from: strDate)!
}

func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = OrignalFormatter
    let strdate: String = dateFormatterGet.string(from: Date)
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = YouWantFormatter
    let date: Date = dateFormatterPrint.date(from: strdate)!
    return dateFormatterPrint.string(from: date)
}
func GetFormatedDateValue(date:Date) -> String
{
    return DateToString(Formatter: "yyyy-MM-dd", date: date)
}
func GetAllMonthsNames() -> [String]
{
    let getMonthArray = Calendar.current
    let arrMonthName = getMonthArray.monthSymbols
    return arrMonthName
}
//MARK: - Set Toaster

func makeToast(strMessage : String){
    
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    
}

//MARK: - Valid Email

func isValidEmail(emailAddressString:String) -> Bool
{
    var returnValue = true
    let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
    
    do {
        let regex = try NSRegularExpression(pattern: emailRegEx)
        let nsString = emailAddressString as NSString
        let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
        
        if results.count == 0
        {
            returnValue = false
        }
        
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        returnValue = false
    }
    
    return  returnValue
}


func setAlert(msg:String,title:String)
{
    var alert = UIAlertView()
    alert.title = title
    alert.message = msg
    alert.show()
    
}


extension UIViewController : NVActivityIndicatorViewable
{
 
    func setupNavigationBarwithTitle(titleText:String)
    {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.black
        HeaderView.font = themeFont(size: 17, fontname: .light)
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationItem.titleView = HeaderView
        
//        let rightButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_notification_header_black"), style: .plain, target: self, action: #selector(btnNotificationTapped))
//        rightButton.tintColor = UIColor.black
//        self.navigationItem.rightBarButtonItem = rightButton
        
//        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
//        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
//        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
//        self.navigationController?.navigationBar.layer.masksToBounds = false
        
    }
    
    @objc func btnNotificationTapped()
    {
        let obj = mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(obj, animated: true)
    }

    func setupNavigationbarwithHomeButton(titleText:String)
    {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_home_icon"), style: .plain, target: self, action: #selector(btnHomeAction))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.black
        HeaderView.font = themeFont(size: 17, fontname: .light)
        
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationItem.titleView = HeaderView
    }
    
    func setupNavigationbarwithBackButton(titleText:String)
    {
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_arrow_back_header"), style: .plain, target: self, action: #selector(backAction))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = UIColor.black
        HeaderView.font = themeFont(size: 17, fontname: .light)
        
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationItem.titleView = HeaderView
        
//        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
//        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
//        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
//        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    @objc func btnHomeAction(){
//        appdelgate.objCustomTabBar.dismiss(animated: false, completion: nil)
        
//        self.dismiss(animated: false, completion: nil)
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SelectPropertyVC") as! SelectPropertyVC
        self.navigationController?.pushViewController(obj, animated: false)
        
        /*for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SelectPropertyVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }*/
    }
    
    @objc func backAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  For Loader NVActivityIndicatorView Process

    func showLoader()
    {
        let LoaderString:String = "Loading..."
        let LoaderType:Int = 29
        let LoaderSize = CGSize(width: 30, height: 30)
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType(rawValue:LoaderType))
        
    }
    
    func hideLoader()
    {
        stopAnimating()
    }
    
    func removeUserDetailsData()
    {
        Defaults.removeObject(forKey: "storedWorkplaceId")
        Defaults.removeObject(forKey: "valueOfarrayWorkplaceList")
        Defaults.removeObject(forKey: "manuallyLoginStatusCheck")
        Defaults.removeObject(forKey: "CurrentTime")
        Defaults.removeObject(forKey: "UserDetail")
        Defaults.synchronize()
        
    }
    
    
}
//MARK:- Get Colors
func GetAllColors() -> [String]
{
    /*
     <color name="bright_pink">#FF007F</color>
     <color name="red">#FF0000</color>
     <color name="orange">#FF7F00</color>
     <color name="yellow">#FFFF00</color>
     <color name="chartreuse">#7FFF00</color>
     <color name="green">#00FF00</color>
     <color name="spring_green">#00FF7F</color>
     <color name="cyan">#00FFFF</color>
     <color name="azure">#007FFF</color>
     <color name="blue">#0000FF</color>
     <color name="violet">#7F00FF</color>
     <color name="magenta">#FF00FF</color>
     <color name="gray">#E6E6E6</color>
     <color name="darkgray">#8D8989</color>
     
     
     
     <array name="rainbow">
     <item>@color/red</item>
     <item>@color/bright_pink</item>
     <item>@color/orange</item>
     <item>@color/green</item>
     <item>@color/yellow</item>
     <item>@color/spring_green</item>
     <item>@color/cyan</item>
     <item>@color/azure</item>
     <item>@color/chartreuse</item>
     <item>@color/blue</item>
     <item>@color/violet</item>
     <item>@color/magenta</item>
     
     
     lightRed
     lightPurple
     sky
     lightGreen
     orange
     brown
     lightBlue
     Blue
     Red
     green
     */
    
    let arrayColors : [String] = ["EC7063","BB8FCE","5DADE2","48C9B0","F39c12","09BAC0","AED6F1","1565C0","FF0033","81C784","957DAD","F694C1","8D8989","E6E6E6"]
    return arrayColors
    
}
func GetAllRGBColor() -> [UIColor]
{
    return [UIColor.appThemeLightRed,UIColor.appThemeLightPurple,UIColor.appThemeSky,UIColor.appThemeLightGreen,UIColor.appThemeOrange,UIColor.appThemeAqua,UIColor.appThemeLightBlue,UIColor.appThemeBlue,UIColor.appThemeRed,UIColor.appThemeGreen,UIColor.appThemeViolet,UIColor.appThemePink,UIColor.appThemeOwnerDarkGray,UIColor.appThemeGray]
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
