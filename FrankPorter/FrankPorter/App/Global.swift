//
//  Global.swift
//  SimpLog
//
//  Created by YASH on 09/07/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON



var localTimeZoneName: String { return TimeZone.current.identifier }
let deviceId = UIDevice.current.identifierForVendor!.uuidString
var isSetMaxCurrentYear = false
var dictSelectedProperty = JSON()
var selectedMinimumYear = 100
var selectedMaximumYear = 101
var progressView = UIView()

func GetToken() -> String
{
    guard let authenticationDetail = UserDefaults.standard.value(forKey: kAuthenticateKey) as? Data else { return "" }
    let dictData = JSON(authenticationDetail)
    return dictData["token"].stringValue
}
//MARK: - UserDetails Store
//MARK : Store data

func getUserDetail(_ forKey: String) -> String
{
    guard let userDetail = UserDefaults.standard.value(forKey: kUserDetailsKey) as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}
func getSubUserDetail(_ forKey: String) -> JSON
{
    guard let userDetail = UserDefaults.standard.value(forKey: kUserDetailsKey) as? Data else { return JSON() }
    let data = JSON(userDetail)
    return data[forKey]
}
