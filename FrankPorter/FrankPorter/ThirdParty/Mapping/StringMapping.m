
#import "StringMapping.h"
#import "FrankPorter-Swift.h"
static StringMapping* sharedStrilgLocalizer = nil;

@implementation StringMapping
@class Basicstuff;
- (id)init {
	
	if(![super init]) return nil;
	
    NSString *stringsPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Language.plist"];
    dictLocalization = [[NSDictionary alloc] initWithContentsOfFile:stringsPath];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    dictStringMappings = [dictLocalization objectForKey:[defaults valueForKey:@"SelectedLanguage"]];
    NSLog(@"Selected language setup - %@",[dictLocalization objectForKey:[defaults valueForKey:@"SelectedLanguage"]]);

    // dictStringMappings = [dictLocalization objectForKey:@"en"];
    return self;
}

+ (StringMapping *)sharedMapping {
	
    @synchronized(self) {
		if(!sharedStrilgLocalizer) {
            
            sharedStrilgLocalizer = [[StringMapping alloc] init];
		}
	}
	return sharedStrilgLocalizer;
}

- (void)setLanguage {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"Selected language setup - %@",[dictLocalization objectForKey:[defaults valueForKey:@"SelectedLanguage"]]);
    dictStringMappings = [dictLocalization objectForKey:[defaults valueForKey:@"SelectedLanguage"]];
   // dictStringMappings = [dictLocalization objectForKey:@"en"];
    
}

- (NSString *)stringForKey:(NSString *)strKey {
    
    NSString *strMapping = [dictStringMappings objectForKey:strKey];
    return strMapping;
}

-(NSString *)alertMsgForKey:(NSString *)strKey
{
    
    NSDictionary *dictAlertgMappings = [dictStringMappings objectForKey:@"AlertMessages"];
    NSString *strMapping = [dictAlertgMappings objectForKey:strKey];
    return strMapping;
}

@end
