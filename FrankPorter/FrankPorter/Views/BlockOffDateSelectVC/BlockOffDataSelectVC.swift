//
//  BlockOffDataSelectVC.swift
//  FrankPorter
//
//  Created by Khushbu on 15/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class BlockOffDataSelectVC: UIViewController {
    
    //MARK:- Outklet Zone

    @IBOutlet var lblHeading : UILabel!
    @IBOutlet var lblFromHeading : UILabel!
    @IBOutlet var lblToHeading : UILabel!

    @IBOutlet var txtFrom : UITextField!
    @IBOutlet var txtTo : UITextField!

    @IBOutlet var btnBlock : UIButton!
    @IBOutlet var btnUpdate : UIButton!
    @IBOutlet var datePicker : UIDatePicker!

    @IBOutlet var btnDone : UIButton!
    @IBOutlet var btnCancel : UIButton!

    @IBOutlet var vwDatePicker : UIView!
    @IBOutlet var vwDatePickerContent : UIView!
    
    @IBOutlet weak var lblFromTimeTitle: UILabel!
    @IBOutlet weak var txtFromTime: CustomTextField!
    
    @IBOutlet weak var lblToTimeTitle: UILabel!
    @IBOutlet weak var txtToTime: CustomTextField!
    @IBOutlet weak var txtViewComment: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
    
    //MARK:- Variable Declaration
    
    var activateTextField:Int = 0
    var notes:String = ""
    var FromDate = Date()
    var ToDate = Date()
    var isSetBlock = true
    var blockOffHandler : (_ fromDate : String,_ toDate : String,_ note:String,_ to:String,_ isUpdate:Bool) -> Void = {_,_,_,_,_ in}
    var isFromSelected = true
    
    let strFormatter = "yyyy-MM-dd"
    let strFormatter1 = "hh:mm a"
    let strFormatter2 = "HH:mm:ss"
    
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    let formatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }()
    
    //MARK:- View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUI()
        
    }
    
    func setUpUI()
    {
        [lblFromHeading,lblToHeading,lblFromTimeTitle,lblToTimeTitle].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 14, fontname: .light)
            
        }
        
        /*[txtFrom,txtTo].forEach { (textfield) in
            textfield?.textColor = UIColor.black
            textfield?.font = themeFont(size: 14, fontname: .light)
            textfield?.layer.cornerRadius = 5
            textfield?.layer.masksToBounds = true
            textfield?.layer.borderWidth = 1
            textfield?.layer.borderColor = UIColor.appThemeGray.withAlphaComponent(0.35).cgColor
            textfield?.backgroundColor = UIColor.appThemeGray.withAlphaComponent(0.65)
            textfield?.leftView = UIView(frame: CGRect(x:0,y:0,width:20,height:textfield?.bounds.height ?? 35))
            textfield?.leftViewMode = .always
            
        }*/
        
        [lblPlaceholder].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .light)
            lbl?.textColor = UIColor.appThemeLightGreyColor
        }
        
        [txtViewComment].forEach { (txtView) in
            txtView?.backgroundColor = UIColor.appThemeBackgroundLightColor
            txtView?.layer.borderColor = (UIColor.appThemeLightGreyColor).cgColor
            txtView?.layer.borderWidth = 1
            txtView?.font = themeFont(size: 17, fontname: .light)
            txtView?.textContainerInset = UIEdgeInsets(top: 8, left: 13, bottom: 6, right: 13)
        }
        
        [txtFrom,txtTo,txtFromTime,txtToTime].forEach { (txtfld) in
            txtfld?.backgroundColor = UIColor.appThemeBackgroundLightColor
            txtfld?.layer.borderColor = (UIColor.appThemeLightGreyColor).cgColor
            txtfld?.layer.borderWidth = 1
//            txtfld?.leftPaddingView = 18
            txtfld?.font = themeFont(size: 17, fontname: .light)
//            txtfld?.delegate = theDelegate
            
        }
        
//        txtFrom.placeholder = getCommonString(key: "Email_key")
//        txtTo.placeholder = getCommonString(key: "Password_key")
        
        lblHeading.textColor = UIColor.black
        lblHeading.font = themeFont(size: 14, fontname: .light)
        
        [btnCancel,btnDone].forEach { (button) in
            button?.setTitleColor(UIColor.white, for: .normal)
            button?.titleLabel?.font = themeFont(size: 14, fontname: .Boook)
        }
        
        vwDatePickerContent.backgroundColor = UIColor.appThemeRed
        datePicker.backgroundColor = UIColor.white

        [btnUpdate,btnBlock].forEach { (btn) in
            btn?.backgroundColor = UIColor.appThemeRed
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 14, fontname: .light)
        }
        
        datePicker.datePickerMode = .date
        vwDatePicker.isHidden = true
        
        txtFrom.text = formatter.string(from: FromDate)
        txtTo.text = formatter.string(from: ToDate)
        
        let df = DateFormatter()
        df.dateFormat = strFormatter1
//        txtFromTime.text = df.string(from: FromDate) == "00:00:00" ? "" : df.string(from: FromDate)
//        txtToTime.text = df.string(from: ToDate) == "00:00:00" ? "" : df.string(from: ToDate)
        txtFromTime.text = df.string(from: FromDate)
        txtToTime.text = df.string(from: ToDate) 
        txtViewComment.text = notes
        txtViewComment.delegate = self
        if(!isSetBlock)
        {
            btnBlock.setTitle("UNBLOCK", for: .normal)
        }else{
            self.btnUpdate.isHidden = true
            btnBlock.setTitle("BLOCK", for: .normal)
        }
        
    }
    
}

//MARK:- UITextview Delegate
extension BlockOffDataSelectVC:UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""{
            lblPlaceholder.isHidden = false
        } else {
            lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK:- Button Action
extension BlockOffDataSelectVC
{
    @IBAction func btnBlockUnblockAction(_ sender : UIButton)
    {
        if txtFrom.text == ""{
            makeToast(strMessage: "Please select from date")
            return
        } else if txtFromTime.text == "" {
            makeToast(strMessage: "Please select from time")
            return
        } else if txtTo.text == "" {
            makeToast(strMessage: "Please select to date")
            return
        } else if txtToTime.text == "" {
            makeToast(strMessage: "Please select to time")
            return
        }

        
        let strFromTime = stringTodate(OrignalFormatter:strFormatter1 , YouWantFormatter: strFormatter2, strDate: self.txtFromTime.text!)
        let strToTime = stringTodate(OrignalFormatter:strFormatter1 , YouWantFormatter: strFormatter2, strDate: self.txtToTime.text!)
        
        let dtFrom = stringTodate(Formatter: strFormatter, strDate: self.txtFrom.text!)
        let dtTo = stringTodate(Formatter: strFormatter, strDate: self.txtTo.text!)
        
        if dtTo < dtFrom || dtTo == Date(){
            makeToast(strMessage: "Please enter valid To date")
            return
        }
        
        let strBlockFromDate = self.txtFrom.text! + " " + strFromTime
        let strBlockToDate = self.txtTo.text! + " " + strToTime
        
        
        let calendar = Calendar.current
        let oneDayAgo = calendar.date(byAdding: .day, value: -1, to: ToDate)
        
        formatter.dateFormat = "yyyy-MM-dd"
        let strOneDayAgo = formatter.string(from: oneDayAgo!)
        
        let strOneDayAgoDate = strOneDayAgo + "T" + strToTime + ".000Z"
        
        
        blockOffHandler(strBlockFromDate,strBlockToDate,txtViewComment.text.trimmingCharacters(in: .whitespaces),strOneDayAgoDate,false)
//        blockOffHandler(FromDate,ToDate)
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func btnCloseAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnFromDateSelectAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
//        if(isSetBlock)
//        {
            activateTextField = 1
//            isFromSelected = true
            vwDatePicker.isHidden = false
            datePicker.datePickerMode = .date
            datePicker.date = FromDate
//        }
    }
    @IBAction func btnToDateSelectAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
//        if(isSetBlock)
//        {
            activateTextField = 2
            datePicker.datePickerMode = .date
//            isFromSelected = false
            vwDatePicker.isHidden = false
            datePicker.minimumDate = FromDate.GetDateAfterDays(days: 1)

            datePicker.date = ToDate

//        }
    }
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        if activateTextField == 1 {
            formatter.dateFormat = "yyyy-MM-dd"
            txtFrom.text = formatter.string(from: datePicker.date)
            FromDate = datePicker.date
        }else if activateTextField == 2{
            formatter.dateFormat = "yyyy-MM-dd"
            datePicker.minimumDate = FromDate
            txtTo.text = formatter.string(from: datePicker.date)
            ToDate = datePicker.date
        } else if activateTextField == 3{
            formatter.dateFormat = strFormatter1
            txtFromTime.text = formatter1.string(from: datePicker.date)
        } else if activateTextField == 4{
            formatter.dateFormat = strFormatter1
            txtToTime.text = formatter1.string(from: datePicker.date)
        }
        vwDatePicker.isHidden = true
        
    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        vwDatePicker.isHidden = true
    }
    
    @IBAction func btnFromTimeAction(_ sender:UIButton){
//        if(isSetBlock)
//        {
        self.view.endEditing(true)
            activateTextField = 3
            vwDatePicker.isHidden = false
            datePicker.datePickerMode = .time
//        }
    }
    
    @IBAction func btnToTimeAction(_ sender:UIButton){
//        if(isSetBlock)
//        {
        self.view.endEditing(true)
            activateTextField = 4
            vwDatePicker.isHidden = false
            datePicker.datePickerMode = .time
//        }
    }
    
    @IBAction func btnUpdateAction(_ sender:UIButton){
        if txtFrom.text == ""{
            makeToast(strMessage: "Please select from date")
            return
        } else if txtFromTime.text == "" {
            makeToast(strMessage: "Please select from time")
            return
        } else if txtTo.text == "" {
            makeToast(strMessage: "Please select to date")
            return
        } else if txtToTime.text == "" {
            makeToast(strMessage: "Please select to time")
            return
        }
        
        let strFromTime = stringTodate(OrignalFormatter:strFormatter1 , YouWantFormatter: strFormatter2, strDate: self.txtFromTime.text!)
        let strToTime = stringTodate(OrignalFormatter:strFormatter1 , YouWantFormatter: strFormatter2, strDate: self.txtToTime.text!)
      
        let dtFrom = stringTodate(Formatter: strFormatter, strDate: self.txtFrom.text!)
        let dtTo = stringTodate(Formatter: strFormatter, strDate: self.txtTo.text!)
        
        if dtTo < dtFrom || dtTo == Date(){
            makeToast(strMessage: "Please enter valid To date")
            return
        }
        
        
        let strBlockFromDate = self.txtFrom.text! + " " + strFromTime
        let strBlockToDate = self.txtTo.text! + " " + strToTime
        
        let calendar = Calendar.current
        let oneDayAgo = calendar.date(byAdding: .day, value: -1, to: ToDate)
        
        formatter.dateFormat = "yyyy-MM-dd"
        let strOneDayAgo = formatter.string(from: oneDayAgo!)
        
        let strOneDayAgoDate = strOneDayAgo + "T" + strToTime + ".000Z"
        blockOffHandler(strBlockFromDate,strBlockToDate,txtViewComment.text,strOneDayAgoDate,true)
        //        blockOffHandler(FromDate,ToDate)
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
