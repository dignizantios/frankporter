//
//  CalendarMonthYearView.swift
//  FrankPorter
//
//  Created by Khushbu on 08/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import DropDown

protocol SelectCalendarComponentDelegate
{
    func NextButtonAction()
    func PreviousButtonAction()
    func SelectYearAction(strYear:String)
    
}


class CalendarMonthYearView: UIView {

    //MARK:- Outlets
    
    @IBOutlet weak var lblMonthName: UILabel!
    @IBOutlet weak var btnYearName: UIButton!
    
    //MARK:- Variables
    var CalendarComponentsDelegate : SelectCalendarComponentDelegate?
    var YearDD = DropDown()
//    var arrMonthName = [String]()
    var currentMonth = 1
    var selectedIndex:Int = 0
    
    override func awakeFromNib() {
        
        lblMonthName.textColor = UIColor.black
        lblMonthName.font = themeFont(size: 15, fontname: .light)
        
        btnYearName.setTitleColor(UIColor.black, for: .normal)
        btnYearName.titleLabel?.textColor = UIColor.black
        
//        let getMonthArray = Calendar.current
//        arrMonthName = getMonthArray.monthSymbols
//        currentMonth = GetCalendarCurrentMonth()
//        lblMonthName.text = arrMonthName[currentMonth-1]

        configureDropDown(dropDown:YearDD, view:btnYearName)
    }
    
    //MARK:-Dropdown Configuration
    func configureDropDown(dropDown:DropDown, view:UIView)
    {
       
        dropDown.anchorView = view
        dropDown.direction = .any
        var array : [String] =  []
        if(isSetMaxCurrentYear)
        {
            array = GetMaxCurrentYearNames()

        }else{
            array = GetYearNames()

        }
        dropDown.dataSource = array

        if(array.contains("\(GetCurrentYear())"))
        {
            let index = array.firstIndex(of: "\(GetCurrentYear())")
            dropDown.selectRow(index ?? 0, scrollPosition: .top)
        }
        
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.selectedIndex = index
            self.btnYearName.setTitle(item, for: .normal)
            self.CalendarComponentsDelegate?.SelectYearAction(strYear:item)
        }
    }
    
    
    //MARK:- Other
    
    func SetCurrentMonthYear(strMonth : String,strYear:String)
    {
        YearDD.clearSelection()
        lblMonthName.text = strMonth
        self.btnYearName.setTitle(strYear, for: .normal)
        let index = YearDD.dataSource.firstIndex(of: "\(strYear)")
        YearDD.selectRow(index ?? 0, scrollPosition: .top)
    }
    
    //MARK:- Action
    @IBAction func btnNextAction(_ sender : UIButton)
    {
        CalendarComponentsDelegate?.NextButtonAction()
    }
    @IBAction func btnPreviousAction(_ sender : UIButton)
    {
        CalendarComponentsDelegate?.PreviousButtonAction()
    }
    @IBAction func btnSelectYearAction(_ sender : UIButton)
    {
        YearDD.show()
    }

}
